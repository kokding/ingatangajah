package ingatan.gajah.com.ingatangajah.repository

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Question(
        @SerializedName("id")
        val id: Int,

        @SerializedName("fullName")
        val fullname: String,

        @SerializedName("image")
        val image: String) : Parcelable