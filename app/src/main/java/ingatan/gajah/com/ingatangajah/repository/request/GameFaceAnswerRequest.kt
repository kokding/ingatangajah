package ingatan.gajah.com.ingatangajah.repository.request

import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.model.Answer
import ingatan.gajah.com.ingatangajah.repository.model.Faces
import ingatan.gajah.com.ingatangajah.repository.response.GameSettings

data class GameFaceAnswerRequest(
        @SerializedName("game_settings")
        val gameSettings: GameSettings,
        @SerializedName("questions")
        val questions: List<Faces>,
        @SerializedName("answers")
        val answers: List<Answer>
)