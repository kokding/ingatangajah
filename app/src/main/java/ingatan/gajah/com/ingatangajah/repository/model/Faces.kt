package ingatan.gajah.com.ingatangajah.repository.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Faces(
        @SerializedName("id")
        val id: Int,

        @SerializedName("fullName")
        val fullname: String,

        @SerializedName("image")
        val image: String) : Parcelable
