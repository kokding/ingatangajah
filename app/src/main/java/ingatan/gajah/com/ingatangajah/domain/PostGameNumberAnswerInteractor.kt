package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.GameFaceAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.request.GameNumberAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameNumberResultResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostGameNumberAnswerInteractor(){
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postGameNumberAnswer(gameId:Int, answer: GameNumberAnswerRequest, token: String): Observable<Response<GameNumberResultResponse>> {

        return apiManager
                .postGameNumberAnswer(
                        gameId = gameId,
                        loginToken = "Bearer "+token,
                        answerRequest = answer
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}