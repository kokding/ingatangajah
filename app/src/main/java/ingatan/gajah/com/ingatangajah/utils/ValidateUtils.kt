package ingatan.gajah.com.ingatangajah.utils

import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import java.util.regex.Pattern

fun validate(vararg views: View): Boolean {
    return validate(null, *views)
}

fun validate(errorMessage: String?, vararg views: View): Boolean {
    var isValid = true
    for (view in views) {
        if (view is EditText) {
            if (!isValid)
                isNotEmpty(view, errorMessage)
            else
                isValid = isNotEmpty(view, errorMessage)
        }
    }
    return isValid
}

fun isNotEmpty(editText: EditText, textInputLayout: TextInputLayout,
               errorMessage: String?): Boolean {
    val text = editText.text.toString().trim { it <= ' ' }
    if (TextUtils.isEmpty(text)) {
        if (errorMessage != null)
            textInputLayout.error = errorMessage
        editText.requestFocus()
        return false
    }
    return true
}

fun isNotEmpty(editText: EditText): Boolean {
    return isNotEmpty(editText, null)
}

fun isNotEmpty(editText: EditText, errorMessage: String?): Boolean {
    val text = editText.text.toString().trim { it <= ' ' }
    if (TextUtils.isEmpty(text)) {
        if (errorMessage != null)
            editText.error = errorMessage
        editText.requestFocus()
        return false
    }
    return true
}

fun passwordConfirmer(etPassword: EditText, etPasswordConfrimation: EditText,
                      textInputLayout: TextInputLayout): Boolean? {
    var isValid: Boolean? = true
    textInputLayout.error = null
    val password = etPassword.text.toString()
    val passwordConfrimation = etPasswordConfrimation.text.toString()
    if (!password.equals(passwordConfrimation, ignoreCase = true)) {
        textInputLayout.error = "Password Tidak Sama"
        isValid = false
    }
    return isValid
}

//NEW VALIDATOR
fun editTextWatcher(tils: Array<TextInputLayout>) {
    var tilCounter = 0
    for (til in tils) {
        val finalTilCounter = tilCounter
        til.editText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                val message: String?
                if (s.length == 0) {
                    tils[finalTilCounter].error = "Wajib Diisi"
                    til.editText!!.requestFocus()
                } else {
                    if (til.editText!!.inputType == AppConstant.INPUT_TYPE_PASSWORD) {
                        message = if (matchValidator("^.{8,}$",
                                        s))
                            null
                        else
                            "Password min. 8 karakter"
                        til.editText!!.requestFocus()
                    } else if (til.editText!!
                                    .inputType == AppConstant.INPUT_TYPE_EMAIL) {
                        message = if (matchValidator("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
                                        s))
                            null
                        else
                            "Format email salah"
                        til.editText!!.requestFocus()
                    } else {
                        message = if (s.length < 4) "Terlalu pendek" else null
                    }
                    tils[finalTilCounter].error = message
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
        tilCounter++
    }
}

fun validate(tils: Array<TextInputLayout>): Boolean? {
    var isValid: Boolean? = true

    for (til in tils) {
        if (TextUtils.isEmpty(til.editText!!.text)) {
            til.error = "Wajib diisi"
            til.editText!!.requestFocus()
            isValid = false
        } else {
            if (til.editText!!.inputType == AppConstant.INPUT_TYPE_PASSWORD) {
                if (!matchValidator("^.{8,}$",
                                til.editText!!.text)) {
                    isValid = false
                }
            } else if (til.editText!!.inputType == AppConstant.INPUT_TYPE_EMAIL) {
                if (!matchValidator("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
                                til.editText!!.text)) {
                    isValid = false
                }
            }
        }
    }
    return isValid
}

private fun matchValidator(patternString: String, s: CharSequence): Boolean {
    val pattern = Pattern.compile(patternString)
    val matcher = pattern.matcher(s)
    return matcher.matches()
}

fun patternMatcher(patternString: String, s: CharSequence): Boolean {
    val pattern = Pattern.compile(patternString)
    val matcher = pattern.matcher(s)
    return matcher.matches()
}