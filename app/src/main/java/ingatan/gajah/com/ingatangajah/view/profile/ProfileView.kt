package ingatan.gajah.com.ingatangajah.view.profile

import ingatan.gajah.com.ingatangajah.repository.response.Data
import ingatan.gajah.com.ingatangajah.repository.response.PostAvatar

interface ProfileView{
    fun showUserCredential(userCredential: Data)
    fun goToSplash()
    fun showAvatarLoading()
    fun hideAvatarLoading()
    fun error(message: String?)
    fun showAvatar(body: PostAvatar)

}