package ingatan.gajah.com.ingatangajah.view.game.number

import ingatan.gajah.com.ingatangajah.repository.response.GameNumberResultResponse

interface GameNumberView{
    fun goToResult(body: GameNumberResultResponse?)
    fun error(message: String?)

}