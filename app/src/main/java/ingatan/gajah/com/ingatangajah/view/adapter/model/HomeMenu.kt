package ingatan.gajah.com.ingatangajah.view.adapter.model

data class HomeMenu(
        var name: String,
        var image: Int,
        var id: Int,
        var isSelected: Boolean = false
)
