package ingatan.gajah.com.ingatangajah.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment() : Fragment(), IBaseView {

    private var mActivity: BaseActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayoutResource(), container, false)

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val activity = context
            this.mActivity = activity
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onViewReady()
    }

    override fun setupToolbar(toolbar: Toolbar, title: String, isChild: Boolean) {
        mActivity?.setupToolbar(toolbar, title, isChild)
    }

    override fun setupToolbar(title: String, isChild: Boolean) {
        mActivity?.setupToolbar(title, isChild)
    }

    override fun setupToolbar(toolbar: Toolbar, isChild: Boolean) {
        mActivity?.setupToolbar(toolbar, isChild)
    }

    override fun onDetach() {
        mActivity = null
        super.onDetach()
    }

    private fun onViewReady() {
        initLib()
        initIntent()
        initUI()
        initAction()
        initProcess()
    }

    fun setFragmentTransaction(containerViewId: Int, fragment: Fragment, addToBackStack: Boolean) {
        val transaction = childFragmentManager.beginTransaction()

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(containerViewId, fragment)
        if (addToBackStack)
            transaction.addToBackStack(null)
        // Commit the transaction
        transaction.commit()
    }

    //    pass Layout here
    protected abstract fun getLayoutResource(): Int

    //    Init Presenter and Component Injection here
    protected abstract fun initLib()

    //    Extract desired intent here
    protected abstract fun initIntent()

    //    initialize the UI, setup toolbar, setText etc here
    protected abstract fun initUI()

    //    initialize UI interaction here
    protected abstract fun initAction()

    //    initialize main Process here e.g call presenter to load data
    protected abstract fun initProcess()
}