package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Meta(
        @Expose
        @SerializedName("limit")
        val limit: Int,
        @Expose
        @SerializedName("offset")
        val offset: Int,
        @Expose
        @SerializedName("total")
        val total: Int)