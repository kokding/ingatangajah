package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.SerializedName

data class PostAvatar(

        @SerializedName("avatar_url")
        val avatarUrl: String)