package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.view.adapter.model.Leaderboard

data class LeaderboardResponse(

        @SerializedName("code")
        val code: Int,

        @SerializedName("status")
        val status: String,

        @SerializedName("meta")
        val meta: Meta,

        @SerializedName("data")
        val leaderboards: List<Leaderboard>)