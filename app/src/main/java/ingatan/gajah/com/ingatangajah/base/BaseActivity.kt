package ingatan.gajah.com.ingatangajah.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.WindowManager

abstract class BaseActivity : AppCompatActivity(), IBaseView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        setContentView(getLayoutResource())
        onViewReady()
    }

    fun onViewReady() {
        initIntent()
        initUI()
        initAction()
    }

    abstract fun getLayoutResource(): Int
    abstract fun initIntent()
    abstract fun initUI()
    abstract fun initAction()

    override fun setupToolbar(toolbar: Toolbar, title: String, isChild: Boolean) {
        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.title = title
            supportActionBar!!.setDisplayHomeAsUpEnabled(isChild)
        }
    }

    override fun setupToolbar(toolbar: Toolbar, isChild: Boolean) {
        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(isChild)
        }
    }

    override fun setupToolbar(title: String, isChild: Boolean) {
        if (supportActionBar != null) {
            supportActionBar!!.title = title
            supportActionBar!!.setDisplayHomeAsUpEnabled(isChild)
        }
    }

    fun setFragmentTransaction(containerViewId: Int, fragment: Fragment, addToBackStack: Boolean) {
        val transaction = supportFragmentManager.beginTransaction()

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(containerViewId, fragment)
        if (addToBackStack)
            transaction.addToBackStack(null)
        // Commit the transaction
        transaction.commit()
    }

//    toast("Hi there!")
//    toast(R.string.message)
//    longToast("Wow, such duration")
//    snackbar(view, "Hi there!")
//    snackbar(view, R.string.message)
//    longSnackbar(view, "Wow, such duration")
//    snackbar(view, "Action, reaction", "Click me!") { doStuff() }

//    alert("Hi, I'm Roy", "Have you tried turning it off and on again?") {
//        yesButton { toast("Oh…") }
//        noButton {}
//    }.show()

//    alert(Appcompat, "Some text message").show()

//    alert {
//        customView {
//            editText()
//        }
//    }.show()

//    val dialog = progressDialog(message = "Please wait a bit…", title = "Fetching data")
}