package ingatan.gajah.com.ingatangajah.view.main

import android.support.design.widget.BottomNavigationView
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.view.home.HomeMenuFragment
import ingatan.gajah.com.ingatangajah.view.leaderboard.LeaderboardFragment
import ingatan.gajah.com.ingatangajah.view.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : BaseActivity(), MainView {

    lateinit var presenter: MainPresenter

    override fun initIntent() {
        presenter = MainPresenter(this)
    }

    override fun initUI() {
        setupToolbar(toolbar, "Home", false)
        setFragmentTransaction(R.id.container_main, HomeMenuFragment(), false)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun initAction() {}


    override fun getLayoutResource(): Int = R.layout.activity_main

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                setupToolbar(toolbar, "Home", false)
                setFragmentTransaction(R.id.container_main, HomeMenuFragment(), false)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_leaderboard -> {
                setupToolbar(toolbar, "Leaderboard", false)
                setFragmentTransaction(R.id.container_main, LeaderboardFragment(), false)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                setupToolbar(toolbar, "Profile", false)
                setFragmentTransaction(R.id.container_main, ProfileFragment(), false)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}
