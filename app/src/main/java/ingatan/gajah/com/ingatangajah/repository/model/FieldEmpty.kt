package ingatan.gajah.com.ingatangajah.repository.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FieldEmpty (

        @Expose
        @SerializedName("field")
        val field: String,
        @Expose
        @SerializedName("message")
        val message: List<String>)
