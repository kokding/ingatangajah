package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import ingatan.gajah.com.ingatangajah.view.adapter.model.Leaderboard
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_score.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor


class LeaderboardAdapter(private val context: Context?, private val data: List<Leaderboard>)
    : RecyclerView.Adapter<LeaderboardAdapter.LeaderboardViewHolder>() {

    val preferenceManager: PreferenceManager = PreferenceManager(context!!)
    lateinit var listener: (Leaderboard) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaderboardViewHolder = LeaderboardViewHolder(LayoutInflater.from(context).inflate(R.layout.item_score, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: LeaderboardViewHolder, position: Int) = holder.bindItem(data[position], listener, preferenceManager.getUserCredential()?.id)

    class LeaderboardViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: Leaderboard, listener: (Leaderboard) -> Unit, id: String?) {
            //init UI
            tv_rank.text = item.rank.toString()
            tv_name.text = item.username
            tv_points.text = "${item.points} pts"
            Glide.with(containerView).load(item.avatarUrl).into(img_profile)
            containerLeaderboard.backgroundColor = if (id.equals(item.userId)) containerView.resources.getColor(R.color.charcoal_grey) else containerView.resources.getColor(R.color.real_white)
            tv_name.textColor = if (id.equals(item.userId)) containerView.resources.getColor(R.color.real_white) else containerView.resources.getColor(R.color.semi_black)
            tv_rank.textColor = if (id.equals(item.userId)) containerView.resources.getColor(R.color.real_white) else containerView.resources.getColor(R.color.slime_green)
        }
    }
}