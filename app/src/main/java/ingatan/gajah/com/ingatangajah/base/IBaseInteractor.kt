package ingatan.gajah.com.ingatangajah.base

import ingatan.gajah.com.ingatangajah.repository.ApiRepository

interface IBaseInteractor{

    fun apiManager()
}