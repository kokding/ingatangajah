package ingatan.gajah.com.ingatangajah.repository.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Answer(
        val id: Int,
        val fullName: String
) : Parcelable
