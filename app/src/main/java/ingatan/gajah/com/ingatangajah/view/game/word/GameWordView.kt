package ingatan.gajah.com.ingatangajah.view.game.word

import ingatan.gajah.com.ingatangajah.repository.response.GameWordResultResponse

interface GameWordView{
    fun error(message: String?)
    fun goToResult(body: GameWordResultResponse?)

}