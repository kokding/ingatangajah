package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_face_navigation.*


class FaceNavigationAdapter(private val context: Context?, private val data: List<GameOrder>)
    : RecyclerView.Adapter<FaceNavigationAdapter.GameOrderViewHolder>() {

    lateinit var listener: (GameOrder, Int) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameOrderViewHolder = GameOrderViewHolder(LayoutInflater.from(context).inflate(R.layout.item_face_navigation, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: GameOrderViewHolder, position: Int) = holder.bindItem(data[position], listener)

    class GameOrderViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: GameOrder, listener: (GameOrder, Int) -> Unit) {
            //init UI
            tvNumber.isSelected = !item.answer.isNullOrBlank()
            tvNumber.text = (adapterPosition + 1).toString()
            containerView.setOnClickListener {
                listener(item, adapterPosition)
            }
        }
    }
}