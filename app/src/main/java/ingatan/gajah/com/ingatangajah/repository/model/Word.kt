package ingatan.gajah.com.ingatangajah.repository.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Word (
        @SerializedName("id")
        val id: Int,

        @SerializedName("name")
        val name: String) : Parcelable
