package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.LoginRequest
import ingatan.gajah.com.ingatangajah.repository.response.LoginResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostLoginInteractor {
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postLogin(loginRequest: LoginRequest, token: String): Observable<Response<LoginResponse>> {

        return apiManager
                .login(loginRequest, "Bearer " + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}