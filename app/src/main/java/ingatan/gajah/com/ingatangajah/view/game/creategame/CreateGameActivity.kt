package ingatan.gajah.com.ingatangajah.view.game.creategame

import android.support.design.widget.TextInputLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.R.array.memorize
import ingatan.gajah.com.ingatangajah.R.array.recall
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.History
import ingatan.gajah.com.ingatangajah.repository.response.*
import ingatan.gajah.com.ingatangajah.utils.MessageFactory
import ingatan.gajah.com.ingatangajah.utils.gone
import ingatan.gajah.com.ingatangajah.utils.setImage
import ingatan.gajah.com.ingatangajah.utils.visible
import ingatan.gajah.com.ingatangajah.view.adapter.HistoryAdapter
import ingatan.gajah.com.ingatangajah.view.game.face.GameFaceActivity
import ingatan.gajah.com.ingatangajah.view.game.number.GameNumberActivity
import ingatan.gajah.com.ingatangajah.view.game.order.GameOrderActivity
import ingatan.gajah.com.ingatangajah.view.game.word.GameWordActivity
import kotlinx.android.synthetic.main.activity_create_game.*
import kotlinx.android.synthetic.main.layout_highest_score.*
import kotlinx.android.synthetic.main.layout_loading_view.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import retrofit2.Response

class CreateGameActivity : BaseActivity(), CreateGameView {
    private lateinit var tils: Array<TextInputLayout>
    private val disposableManager: DisposableManager = DisposableManager()
    private lateinit var presenter: CreateGamePresenter
    private var histories: MutableList<History> = mutableListOf()
    private lateinit var historyAdapter: HistoryAdapter
    private var gameId: Int = 0

    override fun getLayoutResource(): Int = R.layout.activity_create_game

    override fun initIntent() {
        presenter = CreateGamePresenter(this, disposableManager, this@CreateGameActivity)
        gameId = intent.getIntExtra(AppConstant.KEY_GAME_ID, 0)
    }

    override fun initUI() {
        setupToolbar(toolbar, "Create Game", true)
        initGameSetting()
        initHistoryList()
        setImage(this@CreateGameActivity,
                when (gameId) {
                    AppConstant.GAME_FACE -> R.drawable.illustration_no_tebak_wajah_game_history
                    AppConstant.GAME_NUMBER -> R.drawable.illustration_no_tebak_angka_game_history
                    AppConstant.GAME_IMAGE -> R.drawable.illustration_no_tebak_gambar_game_history
                    AppConstant.GAME_WORDS -> R.drawable.illustration_no_tebak_kata_game_history
                    else -> {
                        R.drawable.illustration_no_tebak_gambar_game_history
                    }
                }, imgEmpty)
    }

    private fun initHistoryList() {
        historyAdapter = HistoryAdapter(this@CreateGameActivity, histories)
        historyAdapter.gameId = gameId
        rvHistory.adapter = historyAdapter
        rvHistory.layoutManager = LinearLayoutManager(this@CreateGameActivity)
    }


    private fun initGameSetting() {

    }

    override fun initAction() {
        btnCreate.setOnClickListener { showGameSettingAlert() }
        historyAdapter.listener = {}
    }

    private fun showGameSettingAlert() {
        when (gameId) {
            AppConstant.GAME_FACE ->
                MessageFactory.showGameFaceSettingAlert(
                        context = this@CreateGameActivity,
                        title = null,
                        btnPos = null,
                        positiveListener = object : MessageFactory.Companion.CreateGameDialogActionListener {
                            override fun onSettings(rows: String, columns: String, memorize: String, recall: String) {
                                loadingView.visible()
                                presenter.createGameFace(gameId, rows, columns, memorize, recall)
                            }
                        }
                )
            AppConstant.GAME_NUMBER ->
                MessageFactory.showGameNumberSettingAlert(
                        context = this@CreateGameActivity,
                        title = null,
                        btnPos = null,
                        positiveListener = object : MessageFactory.Companion.CreateGameDialogActionListener {
                            override fun onSettings(rows: String, columns: String, memorize: String, recall: String) {
                                loadingView.visible()
                                presenter.createGameNumber(gameId, rows, columns, memorize, recall)
                            }
                        }
                )
            AppConstant.GAME_IMAGE ->
                MessageFactory.showGameImageSettingAlert(
                        context = this@CreateGameActivity,
                        title = null,
                        btnPos = null,
                        positiveListener = object : MessageFactory.Companion.CreateGameDialogActionListener {
                            override fun onSettings(rows: String, columns: String, memorize: String, recall: String) {
                                loadingView.visible()
                                presenter.createGameImage(gameId, rows, columns, memorize, recall)
                            }
                        }
                )
            AppConstant.GAME_WORDS ->
                MessageFactory.showGameWordSettingAlert(
                        context = this@CreateGameActivity,
                        title = null,
                        btnPos = null,
                        positiveListener = object : MessageFactory.Companion.CreateGameDialogActionListener {
                            override fun onSettings(rows: String, columns: String, memorize: String, recall: String) {
                                loadingView.visible()
                                presenter.createGameWords(gameId, rows, columns, memorize, recall)
                            }
                        }
                )
        }
    }

    override fun error(message: String?) {
        loadingView.gone()
        toast(message ?: "Sever Sibuk, coba beberapa saat lagi")
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        presenter.getLeaderboard(gameId)
        presenter.getHistory(gameId)
    }

    override fun showDataPersonal(personalLeaderboard: PersonalLeaderboard?, userCredential: Data?) {
        tvHighestPoint.text = "${personalLeaderboard?.points} pts"
    }

    override fun hideLoadingHistory() {
        pbHistory.gone()
        rvHistory.visible()
    }

    override fun showLoadingHistory() {
        pbHistory.visible()
        rvHistory.gone()
    }

    override fun showHistoryList(histories: List<History>?, meta: Meta?) {
        if (this.histories.size > 0) {
            this.histories.clear()
        }

        if (histories!!.size > 0) {
            rvHistory.visible()
            containerEmpty.gone()
        } else {
            rvHistory.gone()
            containerEmpty.visible()
        }
        this.histories.addAll(histories!!.toMutableList())
        historyAdapter.notifyDataSetChanged()
    }

    override fun goToGame(question: GameFaceResponse?, randomQuestion: GameFaceResponse?, memorize: String, recall: String) {
        startActivity<GameFaceActivity>(
                AppConstant.KEY_GAME_FACE to question,
                AppConstant.KEY_GAME_FACE_RANDOM to randomQuestion,
                AppConstant.KEY_MEMORIZE to memorize,
                AppConstant.KEY_RECALL to recall)
        finish()
    }

    override fun goToGameNumber(t: Response<GameNumberResponse>, body: GameNumberResponse?, memorize: String, recall: String) {
        startActivity<GameNumberActivity>(
                AppConstant.KEY_GAME_NUMBER to body,
                AppConstant.KEY_MEMORIZE to memorize,
                AppConstant.KEY_RECALL to recall)
        finish()
    }

    override fun goToGameImage(question: GameImageResponse?, randomQuestion : GameImageResponse, memorize: String, recall: String) {
        startActivity<GameOrderActivity>(
                AppConstant.KEY_GAME_IMAGE to question,
                AppConstant.KEY_GAME_IMAGE_RANDOM to randomQuestion,
                AppConstant.KEY_MEMORIZE to memorize,
                AppConstant.KEY_RECALL to recall)
        finish()
    }

    override fun goToGameWord(body: GameWordResponse, wordRandom: GameWordResponse, memorize: String, recall: String) {
        startActivity<GameWordActivity>(
                AppConstant.KEY_GAME_WORDS to body,
                AppConstant.KEY_GAME_WORD_RANDOM to wordRandom,
                AppConstant.KEY_MEMORIZE to memorize,
                AppConstant.KEY_RECALL to recall)
        finish()
    }
}
