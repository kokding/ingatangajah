package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.AnswerResult
import ingatan.gajah.com.ingatangajah.utils.setImage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_face_review.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor


class FaceReviewAdapter(private val context: Context, private val data: List<AnswerResult>)
    : RecyclerView.Adapter<FaceReviewAdapter.AnswerResultViewHolder>() {

    lateinit var listener: (AnswerResult) -> Unit

    var state = AppConstant.STATE_ANSWER

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswerResultViewHolder = AnswerResultViewHolder(LayoutInflater.from(context).inflate(R.layout.item_face_review, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: AnswerResultViewHolder, position: Int) = holder.bindItem(data[position], listener, state)

    override fun getItemViewType(position: Int): Int = position

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    class AnswerResultViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: AnswerResult, listener: (AnswerResult) -> Unit, state: Int) {
            var showAnswer = true
            //set image
            setImage(containerView, item.question.image, pb_img, img_game)
            //show answer when review opened
            when (state) {
                AppConstant.STATE_ANSWER -> showAnswer(item)
                AppConstant.STATE_QUESTION -> showQuestion(item)
            }
//            showAnswer(item)
            tvAnswer.setOnClickListener {
                showAnswer = !showAnswer
                when (showAnswer) {
                    true -> {
                        showAnswer(item)
                    }
                    false -> {
                        showQuestion(item)
                    }
                }
            }
        }

        private fun showQuestion(item: AnswerResult) {
            tvAnswer.text = item.question.fullname
            tvAnswer.backgroundColor = containerView.resources.getColor(R.color.grey_dot)
            tvAnswer.textColor = containerView.resources.getColor(R.color.semi_black)
        }

        fun showAnswer(item: AnswerResult) {
            tvAnswer.backgroundColor = containerView.resources.getColor(R.color.real_white)
            tvAnswer.text = if (item.answer.fullName.equals("-1")) "" else item.answer.fullName
            when (item.status) {
                -1 -> tvAnswer.textColor = containerView.resources.getColor(R.color.grey_dot)
                1 -> tvAnswer.textColor = containerView.resources.getColor(R.color.slime_green)
                0 -> tvAnswer.textColor = containerView.resources.getColor(if (item.answer.fullName.equals("-1")) R.color.grey_dot else R.color.tomato_red)
                else -> {
                    tvAnswer.backgroundColor = containerView.resources.getColor(R.color.real_white)
                }
            }
        }
    }
}