package ingatan.gajah.com.ingatangajah.repository.request

import okhttp3.MultipartBody

data class PostAvatarRequest(
        val avatar: MultipartBody.Part?
)
