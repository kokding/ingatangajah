package ingatan.gajah.com.ingatangajah.repository.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.AnswerResult
import ingatan.gajah.com.ingatangajah.repository.model.AnswerResultImage
import ingatan.gajah.com.ingatangajah.repository.model.GameResult
import kotlinx.android.parcel.Parcelize

@Parcelize
class GameImageResultResponse (
        @SerializedName("code")
        val code: Int,

        @SerializedName("status")
        val status: String,

        @SerializedName("game_settings")
        var gameSettings: GameSettings,

        @SerializedName("game_results")
        var gameResults: GameResult,

        @SerializedName("data")
        val data: List<AnswerResultImage>
) : Parcelable
