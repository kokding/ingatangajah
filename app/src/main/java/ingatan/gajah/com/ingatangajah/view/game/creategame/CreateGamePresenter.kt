package ingatan.gajah.com.ingatangajah.view.game.creategame

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.*
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.Faces
import ingatan.gajah.com.ingatangajah.repository.model.Word
import ingatan.gajah.com.ingatangajah.repository.request.ImageRandomRequest
import ingatan.gajah.com.ingatangajah.repository.request.RandomRequest
import ingatan.gajah.com.ingatangajah.repository.request.WordRandomRequest
import ingatan.gajah.com.ingatangajah.repository.response.*
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver
import retrofit2.Response

class CreateGamePresenter(val view: CreateGameView, val disposableManager: DisposableManager, val context: Context) {
    val gameFaceQuestionInteractor: GetGameFaceQuestionInteractor = GetGameFaceQuestionInteractor()
    val gameNumberQuestionInteractor: GetGameNumberQuestionInteractor = GetGameNumberQuestionInteractor()
    val getFaceRandomQuestionInteractor: PostRandomFaceQuestionInteractor = PostRandomFaceQuestionInteractor()
    val getImageQuestionInteractor: GetGameImageQuestionInteractor = GetGameImageQuestionInteractor()
    val getImagePostRandomImageQuestionInteractor: PostRandomImageQuestionInteractor = PostRandomImageQuestionInteractor()
    val getWordQuestionInteractor: GetGameWordQuestionInteractor = GetGameWordQuestionInteractor()
    val getWordPostRandomImageQuestionInteractor: PostGameWordRandomInteractor = PostGameWordRandomInteractor()
    val getPersonalLeaderboardInteractor: GetPersonalLeaderboardInteractor = GetPersonalLeaderboardInteractor()
    val getHistoryInteractor: GetGameHistoryInteractor = GetGameHistoryInteractor()

    val preferenceManager: PreferenceManager = PreferenceManager(context)
    lateinit var memorize: String
    lateinit var recall: String
    lateinit var questions: List<Faces>
    lateinit var imageQuestions: List<Order>
    lateinit var wordQuestions: List<Word>

    fun getLeaderboard(gameId: Int?) {
        val dispose = getPersonalLeaderboardInteractor.getLeaderboard(gameId, "Bearer " + preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<PersonalLeaderboardRsponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<PersonalLeaderboardRsponse>) {
//                view.hideLoading()
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.showDataPersonal(t.body()?.personalLeaderboard, preferenceManager.getUserCredential())
                    }
                }
            }

            override fun onError(e: Throwable) {
//                view.hideLoading()
                view.error(e.message)
            }
        })
        disposableManager.add(dispose)
    }

    fun getHistory(gameId: Int?) {
        view.showLoadingHistory()
        val dispose = getHistoryInteractor.getGameHistory(gameId ?: 3, "Bearer " + preferenceManager.getAnonymousToken(), 3).subscribeWith(object : DisposableObserver<Response<HistoryResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<HistoryResponse>) {
                view.hideLoadingHistory()
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.showHistoryList(t.body()?.histories, t.body()?.meta)
                    }
                }
            }

            override fun onError(e: Throwable) {
                view.hideLoadingHistory()
                view.error(e.message)
            }
        })
        disposableManager.add(dispose)
    }

    fun createGameFace(game: Int, rows: String, columns: String, memorize: String, recall: String) {
        this.memorize = memorize
        this.recall = recall
        val dispose = gameFaceQuestionInteractor.getFaceGameQuestion(
                game = game,
                token = "Bearer ${preferenceManager.getAnonymousToken()}",
                columns = columns,
                rows = rows,
                memorizeTime = (memorize.toInt()*60).toString(),
                recall = (recall.toInt()*60).toString()).subscribeWith(object : DisposableObserver<Response<GameFaceResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameFaceResponse>) {
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        questions = t.body()?.data!!
                        postRandom(questions, t.body()!!)
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }

    fun postRandom(data: List<Faces>, body: GameFaceResponse) {
        val request: RandomRequest
        request = RandomRequest(data)
        val dispose = getFaceRandomQuestionInteractor.postRandomFaceQuestion(
                game = 3,
                token = "Bearer ${preferenceManager.getAnonymousToken()}",
                request = request).subscribeWith(object : DisposableObserver<Response<GameFaceResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameFaceResponse>) {
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {

                        val gameFaceResponse: GameFaceResponse = t.body()!!
                        gameFaceResponse.gameSettings = body.gameSettings
                        view.goToGame(body, t.body(), memorize, recall)
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }

    fun createGameNumber(gameId: Int, rows: String, columns: String, memorize: String, recall: String) {
        val dispose = gameNumberQuestionInteractor.getGameNumberQuestion(
                game = gameId,
                token = "Bearer ${preferenceManager.getAnonymousToken()}",
                columns = columns,
                rows = rows,
                memorizeTime = (memorize.toInt()*60).toString(),
                recall = (recall.toInt()*60).toString()).subscribeWith(object : DisposableObserver<Response<GameNumberResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameNumberResponse>) {
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.goToGameNumber(t, t.body(), memorize, recall)
                    }
                }
            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }

    fun createGameImage(gameId: Int, rows: String, columns: String, memorize: String, recall: String) {
        this.memorize = memorize
        this.recall = recall
        val dispose = getImageQuestionInteractor.getGameImageQuestion(
                game = gameId,
                token = "Bearer ${preferenceManager.getAnonymousToken()}",
                columns = columns,
                rows = rows,
                memorizeTime = (memorize.toInt()*60).toString(),
                recall = (recall.toInt()*60).toString()).subscribeWith(object : DisposableObserver<Response<GameImageResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameImageResponse>) {
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        imageQuestions = t.body()?.data!!
                        postRandomImage(imageQuestions, t.body()!!)
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }

    fun postRandomImage(data: List<Order>, body: GameImageResponse){
        val request: ImageRandomRequest
        request = ImageRandomRequest(data)
        val dispose = getImagePostRandomImageQuestionInteractor.postRandomImageQuestion(
                game = AppConstant.GAME_IMAGE,
                token = "Bearer ${preferenceManager.getAnonymousToken()}",
                request = request).subscribeWith(object : DisposableObserver<Response<GameImageResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameImageResponse>) {
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {

                        val gameImageResponse: GameImageResponse = t.body()!!
                        gameImageResponse.gameSettings = body.gameSettings
                        view.goToGameImage(body, gameImageResponse, memorize, recall)
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }

    fun createGameWords(gameId: Int, rows: String, columns: String, memorize: String, recall: String) {
        this.memorize = memorize
        this.recall = recall
        val dispose = getWordQuestionInteractor.getGameWordQuestion(
                game = gameId,
                token = "Bearer ${preferenceManager.getAnonymousToken()}",
                columns = columns,
                rows = rows,
                memorizeTime = (memorize.toInt()*60).toString(),
                recall = (recall.toInt()*60).toString()).subscribeWith(object : DisposableObserver<Response<GameWordResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameWordResponse>) {
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        wordQuestions = t.body()?.data!!
                        view.goToGameWord(t.body()!!, t.body()!!, memorize, recall)
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)

    }

    private fun postRandomWord(wordQuestions: List<Word>, body: GameWordResponse) {
        val request: WordRandomRequest
        request = WordRandomRequest(wordQuestions)
        val dispose = getWordPostRandomImageQuestionInteractor.postRandomWordQuestion(
                game = AppConstant.GAME_IMAGE,
                token = "Bearer ${preferenceManager.getAnonymousToken()}",
                request = request).subscribeWith(object : DisposableObserver<Response<GameWordResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameWordResponse>) {
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {

                        val wordRandom: GameWordResponse = t.body()!!
                        wordRandom.gameSettings = body.gameSettings
                        view.goToGameWord(body, wordRandom, memorize, recall)
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }
}