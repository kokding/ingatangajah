package ingatan.gajah.com.ingatangajah.repository.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.response.Order
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AnswerResultWord(
        @SerializedName("id")
        val id: Int,

        @SerializedName("question")
        val question: Word,

        @SerializedName("answer")
        val answer: Word,

        @SerializedName("status")
        val status: Int
) : Parcelable
