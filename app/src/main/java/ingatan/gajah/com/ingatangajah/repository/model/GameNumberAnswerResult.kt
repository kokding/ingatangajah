package ingatan.gajah.com.ingatangajah.repository.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameNumberAnswerResult(

        @SerializedName("id")
        val id: Int,
        @SerializedName("question")
        val question: Int,
        @SerializedName("answer")
        val answer: Int,
        @SerializedName("status")
        val status: Int) : Parcelable
