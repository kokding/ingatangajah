package ingatan.gajah.com.ingatangajah.view.profile

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Environment
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import id.zelory.compressor.Compressor
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.repository.request.PostAvatarRequest
import ingatan.gajah.com.ingatangajah.repository.response.Data
import ingatan.gajah.com.ingatangajah.repository.response.PostAvatar
import ingatan.gajah.com.ingatangajah.utils.setImage
import ingatan.gajah.com.ingatangajah.view.splash.SplashScreenActivity
import kotlinx.android.synthetic.main.fragment_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.support.v4.startActivity
import java.io.File
import java.io.IOException


class ProfileFragment : BaseFragment(), ProfileView {

    private lateinit var presenter: ProfilePresenter
    private lateinit var disposableManager: DisposableManager
    private lateinit var avatarImg: Image
    private lateinit var compressedAvatar: File
    override fun getLayoutResource(): Int = R.layout.fragment_profile

    override fun initIntent() {
        disposableManager = DisposableManager()
        presenter = ProfilePresenter(this, disposableManager, context)
        presenter.getCredential()
    }

    override fun initLib() {

    }

    override fun initUI() {

    }

    override fun initAction() {
        btnSignOut.setOnClickListener { presenter.signout() }
        imgProfile.setOnClickListener {
            ImagePicker.create(this)
                    .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                    .folderMode(true) // folder mode (false by default)
                    .toolbarFolderTitle("Folder") // folder selection title
                    .toolbarImageTitle("Tap to select") // image selection title
                    .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
                    .includeVideo(true) // Show video on image picker
                    .single() // single mode
                    .showCamera(true) // show camera or not (true by default)
                    .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default) e
                    .enableLog(true) // disabling log
                    .start() // start image picker activity with request code
        }
    }

    override fun initProcess() {
    }

    override fun showUserCredential(userCredential: Data) {
        setImage(context!!, userCredential.profile?.avatar
                ?: "", R.drawable.icons_ingatan_gajah, imgProfile)
        tvName.text = userCredential.profile?.username
        tvEmail.text = userCredential.profile?.email
    }

    override fun goToSplash() {
        startActivity<SplashScreenActivity>()
        activity?.finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            avatarImg = ImagePicker.getFirstImageOrNull(data)

            compressImage(avatarImg.path)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun compressImage(avatarImg: String) {
        val imageFile = File(avatarImg)
        Compressor(context)
                .setQuality(75)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath())
                .compressToFile(imageFile)
        try {
            compressedAvatar = Compressor(context).compressToFile(imageFile)
            postCompressedImageFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun postCompressedImageFile() {
        val avatarImage = RequestBody
                .create(MediaType.parse("image/jpeg"), compressedAvatar)
        val bodyAvatarImage = MultipartBody.Part
                .createFormData("avatar", compressedAvatar.getName(), avatarImage)
        val postAvatarRequest = PostAvatarRequest(
                bodyAvatarImage)
        presenter.uploadAvatar(postAvatarRequest)
    }

    override fun showAvatarLoading() {
    }

    override fun hideAvatarLoading() {
    }

    override fun error(message: String?) {
    }

    override fun showAvatar(body: PostAvatar) {
        setImage(context!!, avatarImg.path, R.drawable.icons_ingatan_gajah, imgProfile)
    }

    override fun onPause() {
        super.onPause()
        disposableManager.dispose()
    }
}
