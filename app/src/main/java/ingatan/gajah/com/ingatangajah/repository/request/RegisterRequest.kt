package ingatan.gajah.com.ingatangajah.repository.request

import com.google.gson.annotations.SerializedName

data class RegisterRequest(
        val fullName: String,
        val username: String,
        @SerializedName("birth_of_date")
        val birthdate: String,
        val email: String,
        val password: String,
        @SerializedName("password_confirmation")
        val passwordConfirmation: String
)
