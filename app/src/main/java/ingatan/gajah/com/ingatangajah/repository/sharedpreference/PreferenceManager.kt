package ingatan.gajah.com.ingatangajah.repository.sharedpreference

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import ingatan.gajah.com.ingatangajah.repository.response.Data

class PreferenceManager(val context: Context) {

    val PREF = "PREF"
    val PREF_ANONYMOUS = "PREF_ANONYMOUS"
    val PREF_CREDENTIAL = "PREF_CREDENTIAL"

    val gson = Gson()

    var prefs: SharedPreferences = context.getSharedPreferences(PREF, 0)

    fun setAnonymousToken(token: String) {
        prefs.edit().putString(PREF_ANONYMOUS, token).apply()
    }

    fun getAnonymousToken(): String = prefs.getString(PREF_ANONYMOUS, "") ?: ""

    fun setUserCredential(response: Data) {
        val serializedObject = gson.toJson(response)
        prefs.edit().putString(PREF_CREDENTIAL, serializedObject).apply()
    }

    fun getUserCredential(): Data? {
        val credentials: String? = prefs.getString(PREF_CREDENTIAL, "")
        return if (credentials?.isNotEmpty()!!) gson.fromJson<Data>(credentials, Data::class.java) else null
    }

    fun notLoggedIn(): Boolean {
        return getUserCredential()?.id.isNullOrEmpty()
    }

}