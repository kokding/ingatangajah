package ingatan.gajah.com.ingatangajah.view.adapter.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Leaderboard(

        @Expose
        @SerializedName("rank")
        val rank: Int,
        @Expose
        @SerializedName("user_id")
        val userId: String,
        @Expose
        @SerializedName("avatar_url")
        val avatarUrl: String,
        @Expose
        @SerializedName("fullName")
        val fullname: String,
        @Expose
        @SerializedName("username")
        val username: String,
        @Expose
        @SerializedName("points")
        val points: Int)
