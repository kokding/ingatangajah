package ingatan.gajah.com.ingatangajah.view.adapter.viewpager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResponse
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import ingatan.gajah.com.ingatangajah.view.game.word.WordFragment
import org.jetbrains.anko.collections.forEachWithIndex

class ViewPagerAdapter(fragmentManager: FragmentManager,
                       private val words: GameWordResponse,
                       private val gameQuestion: List<GameOrder>,
                       private val state: Int,
                       private val columns: Int) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        var wordQuestion : MutableList<GameOrder> = mutableListOf()
        gameQuestion.forEachWithIndex{
            i, it -> run{
            if(i > (position*words.gameSettings.rows)-1 && i < (position+1)*words.gameSettings.rows){
                wordQuestion.add(it)
            }

        }

        }
        return WordFragment.newInstance(words,state,position, wordQuestion)
    }

    override fun getCount(): Int = columns

}