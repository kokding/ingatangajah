package ingatan.gajah.com.ingatangajah.repository.request

import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.model.Faces
import ingatan.gajah.com.ingatangajah.repository.response.Order

data class ImageRandomRequest(
        @SerializedName("questions")
        val questions: List<Order>
)