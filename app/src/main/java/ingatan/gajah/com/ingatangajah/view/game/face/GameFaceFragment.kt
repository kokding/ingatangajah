package ingatan.gajah.com.ingatangajah.view.game.face


import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.Answer
import ingatan.gajah.com.ingatangajah.repository.request.GameFaceAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse
import ingatan.gajah.com.ingatangajah.utils.MessageFactory
import ingatan.gajah.com.ingatangajah.view.adapter.GameFaceAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.GameOrderAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import kotlinx.android.synthetic.main.fragment_game_face.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.info
import org.jetbrains.anko.support.v4.progressDialog
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast


class GameFaceFragment : BaseFragment(), GameFaceView, GameFaceActivity.GameEndListener, AnkoLogger {

    var gameOrder: MutableList<GameOrder> = mutableListOf()
    var answers: MutableList<Answer> = mutableListOf()
    private val disposableManager = DisposableManager()
    lateinit var adapter: GameFaceAdapter
    private lateinit var presenter: GameFacePresenter
    lateinit var data: GameFaceResponse
    private var state: Int = AppConstant.STATE_MEMORIZE

    companion object {

        fun newInstance(category: GameFaceResponse, state: Int) = GameFaceFragment().apply {
            arguments = bundleOf(
                    AppConstant.KEY_GAME_FACE to category,
                    AppConstant.KEY_GAME_STATE to state)
        }
    }

    override fun getLayoutResource(): Int = R.layout.fragment_game_face

    override fun initLib() {
        presenter = GameFacePresenter(this, disposableManager, context!!)
        GameFaceActivity.setGameListener(this)
    }

    override fun initIntent() {
        data = arguments?.getParcelable(AppConstant.KEY_GAME_FACE)!!
        state = arguments?.getInt(AppConstant.KEY_GAME_STATE, AppConstant.STATE_MEMORIZE)!!
    }

    override fun initUI() {
        adapter = GameFaceAdapter(context, gameOrder)
        adapter.setHasStableIds(true)
        adapter.state = state
        when (state) {
            AppConstant.STATE_MEMORIZE, AppConstant.STATE_RECALL -> {
                rvGameFace.layoutManager = GridLayoutManager(context, data.gameSettings.columns)
            }
            else -> {
                rvGameFace.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                val snapHelper = LinearSnapHelper()
                snapHelper.attachToRecyclerView(rvGameFace)
            }
        }

        rvGameFace.adapter = adapter

        iniData()
    }

    private fun iniData() {
        data.data.forEach {
            gameOrder.add(GameOrder(
                    id = it.id,
                    img = it.image,
                    question = it.fullname,
                    answer = ""))
        }

        adapter.notifyDataSetChanged()
    }

    override fun initAction() {
        adapter.listener = {

        }
    }

    override fun initProcess() {
    }

    override fun onGameEnd(userMemorizeTime: Long, userRecallTime: Long) {
        MessageFactory.showDoneAlert(context,
                getString(R.string.title_end_game),
                getString(R.string.message_end_game_to_result),
                getString(R.string.btn_end_game_to_result),
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {
//                        toast(gameOrder.toString())
                        gameOrder.forEach {
                            answers.add(Answer(it.id,
                                    if (it.answer.isNullOrBlank()) "-1"
                                    else it.answer ?: "-1"))
                        }
                        data.gameSettings.userMemorize = userMemorizeTime.toInt()
                        data.gameSettings.userRecall = userRecallTime.toInt()
                        val answer = GameFaceAnswerRequest(data.gameSettings, data.data, answers)
                        info("MANTAP " + answer.toString())
                        presenter.postAnswer(answer)
                    }

                    override fun onNegativeAnswer() {
                    }
                })
    }

    override fun onPause() {
        super.onPause()
        disposableManager.dispose()
    }

    override fun goToResult(body: GameFaceResultResponse?) {
        startActivity<FaceResultActivity>(AppConstant.KEY_RESULT to body)
        activity?.finish()
    }

    override fun onVisibleMemorizeClicked(isHide: Boolean) {
        adapter.isHide = isHide
        adapter.notifyDataSetChanged()
    }

    override fun error(message: String?) {
        toast(message ?: "Terjadi Kesalahan, Coba Beberapa saat lagi")
    }
}
