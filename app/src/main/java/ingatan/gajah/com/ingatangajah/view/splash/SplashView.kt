package ingatan.gajah.com.ingatangajah.view.splash

interface SplashView{
    fun goToLogin(header: String?)
    fun goToMainActivity()

}