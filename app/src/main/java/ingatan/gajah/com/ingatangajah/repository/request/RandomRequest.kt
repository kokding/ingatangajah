package ingatan.gajah.com.ingatangajah.repository.request

import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.model.Faces
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder

data class RandomRequest (
        @SerializedName("questions")
        val questions: List<Faces>
)

