package ingatan.gajah.com.ingatangajah.view.adapter

import android.app.ProgressDialog.show
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.AnswerResult
import ingatan.gajah.com.ingatangajah.repository.model.AnswerResultWord
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_word_review.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor


class GameWordReviewAdapter(private val context: Context?, private val data: List<AnswerResultWord>)
    : RecyclerView.Adapter<GameWordReviewAdapter.AnswerResultWordViewHolder>() {

    lateinit var listener: (AnswerResultWord) -> Unit

    var state = AppConstant.STATE_ANSWER

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswerResultWordViewHolder = AnswerResultWordViewHolder(LayoutInflater.from(context).inflate(R.layout.item_word_review, parent, false))

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int = position

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: AnswerResultWordViewHolder, position: Int) = holder.bindItem(data[position], listener, state)

    class AnswerResultWordViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: AnswerResultWord, listener: (AnswerResultWord) -> Unit, state: Int) {
            tvLabel.text = "${adapterPosition+1}"
            //init UI
            var showAnswer = true
            //show answer when review opened
            when (state) {
                AppConstant.STATE_ANSWER -> showAnswer(item)
                AppConstant.STATE_QUESTION -> showQuestion(item)
            }
//            showAnswer(item)
            tvAnswer.setOnClickListener {
                showAnswer = !showAnswer
                when (showAnswer) {
                    true -> {
                        showAnswer(item)
                    }
                    false -> {
                        showQuestion(item)
                    }
                }
            }
        }

        fun showQuestion(item: AnswerResultWord) {
            tvAnswer.text = item.question.name
            tvAnswer.backgroundColor = containerView.resources.getColor(R.color.grey_dot)
            tvAnswer.textColor = containerView.resources.getColor(R.color.semi_black)
        }

        fun showAnswer(item: AnswerResultWord) {
            tvAnswer.backgroundColor = containerView.resources.getColor(R.color.real_white)
            tvAnswer.text = if (item.answer.name.equals("-1")) "" else item.answer.name
            when (item.answer.name) {
                "-1" -> tvAnswer.textColor = containerView.resources.getColor(R.color.grey_dot)
                else -> {
                    if (item.answer.name.equals(item.question.name))
                        tvAnswer.textColor = containerView.resources.getColor(R.color.slime_green)
                    else
                        tvAnswer.textColor = containerView.resources.getColor(R.color.tomato_red)
                }
            }
        }
    }
}