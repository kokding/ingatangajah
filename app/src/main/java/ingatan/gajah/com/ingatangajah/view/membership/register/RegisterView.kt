package ingatan.gajah.com.ingatangajah.view.membership.register

import ingatan.gajah.com.ingatangajah.repository.model.FieldEmpty

interface RegisterView{
    fun error(message: String?)
    fun goToLogin()
    fun handleInvalidData(data: List<FieldEmpty>?)

}