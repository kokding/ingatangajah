package ingatan.gajah.com.ingatangajah.view.profile

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.GetProfileInteractor
import ingatan.gajah.com.ingatangajah.domain.PostAvatarInteractor
import ingatan.gajah.com.ingatangajah.repository.request.PostAvatarRequest
import ingatan.gajah.com.ingatangajah.repository.response.Data
import ingatan.gajah.com.ingatangajah.repository.response.PostAvatarResponse
import ingatan.gajah.com.ingatangajah.repository.response.Profile
import ingatan.gajah.com.ingatangajah.repository.response.ProfileResponse
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver
import retrofit2.Response

class ProfilePresenter(val view: ProfileView, val disposableManager: DisposableManager, context: Context?) {
    private val preferenceManager: PreferenceManager = PreferenceManager(context!!)
    private val postAvatarInteractor: PostAvatarInteractor = PostAvatarInteractor()
    private val getProfileInteractor: GetProfileInteractor = GetProfileInteractor()

    fun getCredential() {
        val dispose = getProfileInteractor.getProfile("Bearer " + preferenceManager.getAnonymousToken())
                .subscribeWith(object : DisposableObserver<Response<ProfileResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: Response<ProfileResponse>) {
                        view.hideAvatarLoading()
                        when (t.code()) {
                            400, 401, 404, 204 -> view.error(t.message())
                            else -> {
                                val credentialData = Data(
                                        id = t.body()?.profile?.userId,
                                        email = t.body()?.profile?.email,
                                        profile = t.body()?.profile,
                                        isVerified = 1,
                                        isSuspend = 1,
                                        createdAt = "123456789")
                                preferenceManager.setUserCredential(credentialData)
                                view.showUserCredential(preferenceManager.getUserCredential()!!)
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                    }
                })
        disposableManager.add(dispose)
    }

    fun signout() {
        val profile: Profile? = null
        val data = Data("", "", 0, 0, "", profile)
        preferenceManager.setUserCredential(data)
        view.goToSplash()
    }

    fun uploadAvatar(postAvatarRequest: PostAvatarRequest) {
        view.showAvatarLoading()
        val dispose = postAvatarInteractor
                .postAvatar("Bearer " + preferenceManager.getAnonymousToken(), postAvatarRequest)
                .subscribeWith(object : DisposableObserver<Response<PostAvatarResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: Response<PostAvatarResponse>) {
                        view.hideAvatarLoading()
                        when (t.code()) {
                            400, 401, 404, 204 -> view.error(t.message())
                            else -> {
                                preferenceManager.getUserCredential()?.profile?.avatar = t.body()!!.postAvatar.avatarUrl
                                view.showAvatar(t.body()!!.postAvatar)
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        view.hideAvatarLoading()
                        view.error(e.message)
                    }
                })
        disposableManager.add(dispose)
    }

}