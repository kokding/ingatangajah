package ingatan.gajah.com.ingatangajah.view.membership.register

import android.app.DatePickerDialog
import android.support.design.widget.TextInputLayout
import android.view.MenuItem
import android.widget.DatePicker
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.repository.model.FieldEmpty
import ingatan.gajah.com.ingatangajah.utils.MessageFactory
import ingatan.gajah.com.ingatangajah.utils.editTextWatcher
import ingatan.gajah.com.ingatangajah.utils.passwordConfirmer
import ingatan.gajah.com.ingatangajah.utils.validate
import ingatan.gajah.com.ingatangajah.view.membership.login.LoginActivity
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class RegisterActivity : BaseActivity(), RegisterView , DatePickerDialog.OnDateSetListener{

    private lateinit var presenter: RegisterPresenter
    private lateinit var field: Array<TextInputLayout>
    private lateinit var disposableManager: DisposableManager
    override fun initAction() {
        btnRegist.setOnClickListener {
            validateRegisterData()
        }
        tilBirthday.editText?.setOnClickListener { openDatePicker() }
    }

    private fun openDatePicker() {
        val dialog = DatePickerDialog(this, this, 1993, 1, 1)
        dialog.show()
    }

    private fun validateRegisterData() {
        if (validate(field)!!) {
            if (!passwordConfirmer(tilPassword.editText!!,
                            tilConfirmPassword.editText!!, tilConfirmPassword)!!) return
            val fullname = tilFullName.editText?.text.toString()
            val username = tilUsername.editText?.text.toString()
            val birthdate = tilBirthday.editText?.text.toString()
            val email = tilEmail.editText?.text.toString()
            val password = tilPassword.editText?.text.toString()
            val passwordConfirmation = tilConfirmPassword.editText?.text.toString()
            presenter.postRegister(fullname, username, birthdate, email, password, passwordConfirmation)
        }
    }

    override fun initUI() {
        setupToolbar(toolbar, "Register", true)
    }

    override fun initIntent() {
        disposableManager = DisposableManager()
        presenter = RegisterPresenter(this, disposableManager, this@RegisterActivity)
        field = arrayOf(tilFullName, tilUsername, tilBirthday, tilEmail, tilPassword, tilConfirmPassword)
        editTextWatcher(field)
    }

    override fun getLayoutResource(): Int = R.layout.activity_register

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun error(message: String?) {
        toast(message ?: "Terjadi kesalahan, coba beberapa saat lagi")
    }

    override fun goToLogin() {
        MessageFactory.showDoneAlert(this@RegisterActivity,
                "Berhasil", "Anda berhasil terdaftar. Silahkan login", "OK", object : MessageFactory.Companion.MyDialogActionListener {
            override fun onPositiveAnswer() {
                startActivity<LoginActivity>()
                finish()
            }

            override fun onNegativeAnswer() {
            }

        })
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, date: Int) {
        val trueMonth = month+1
        val monthString = if (trueMonth<10) "0$trueMonth" else "$trueMonth"
        val dateString = if (date<10) "0$date" else "$date"
        tilBirthday.editText?.setText("$year-$monthString-$dateString")
    }

    override fun handleInvalidData(data: List<FieldEmpty>?) {
        data?.forEach {
            when(it.field){
                "email" -> tilEmail.error= it.message[0]
                "username" -> tilUsername.error= it.message[0]
            }
        }

    }
}
