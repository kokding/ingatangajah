package ingatan.gajah.com.ingatangajah.repository.request

import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.model.Word
import ingatan.gajah.com.ingatangajah.repository.response.GameSettings
import ingatan.gajah.com.ingatangajah.repository.response.Order

data class WordRandomRequest (
        @SerializedName("questions")
        val questions: List<Word>
)
