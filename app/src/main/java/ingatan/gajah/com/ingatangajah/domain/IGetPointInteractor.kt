package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.model.Schedule
import io.reactivex.disposables.Disposable

interface IGetPointInteractor{

    fun onResponse(schedules: List<Schedule>)
    fun onFailed(error: Throwable?)
    fun getUserCredential()
}