package ingatan.gajah.com.ingatangajah.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatSeekBar
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.*
import ingatan.gajah.com.ingatangajah.R
import org.jetbrains.anko.find
import org.jetbrains.anko.layoutInflater

class MessageFactory {

    companion object {
        fun showConfirmationAlert(context: Context?, title: String, description: String, btnPos: String,
                                  positiveListener: MyDialogActionListener, btnNeg: String,
                                  negativeListener: MyDialogActionListener) {

            val builder = android.support.v7.app.AlertDialog.Builder(ContextThemeWrapper(context, R.style.myDialog))
            val dialogView = context?.layoutInflater?.inflate(R.layout.alert_confirmation, null)
            builder.setView(dialogView)
            val tvTitle = dialogView?.find<TextView>(R.id.tvTitle)
            val tvDescription = dialogView?.find<TextView>(R.id.tvDesc)
            val btnNegative = dialogView?.find<AppCompatButton>(R.id.btnNegavite)
            val btnPositive = dialogView?.find<AppCompatButton>(R.id.btnPositive)
            val dialog = builder.create()
            tvTitle?.text = title
            tvDescription?.text = description
            btnNegative?.text = btnNeg
            btnPositive?.text = btnPos
            btnPositive?.setOnClickListener {
                dialog.dismiss()
                positiveListener.onPositiveAnswer()
            }
            btnNegative?.setOnClickListener {
                dialog.dismiss()
                negativeListener.onNegativeAnswer()
            }
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.show()
        }

        fun showDoneAlert(context: Context?, title: String, description: String, btnPos: String,
                          positiveListener: MyDialogActionListener) {

            val builder = android.support.v7.app.AlertDialog.Builder(ContextThemeWrapper(context, R.style.myDialog))
            val dialogView = context?.layoutInflater?.inflate(R.layout.alert_done, null)
            builder.setView(dialogView)
            val tvTitle = dialogView?.find<TextView>(R.id.tvTitle)
            val tvDescription = dialogView?.find<TextView>(R.id.tvDesc)
            val btnPositive = dialogView?.find<AppCompatButton>(R.id.btnPositive)
            val dialog = builder.create()
            tvTitle?.text = title
            tvDescription?.text = description
            btnPositive?.text = btnPos
            btnPositive?.setOnClickListener {
                dialog.dismiss()
                positiveListener.onPositiveAnswer()
            }
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.show()
        }

        fun showGameFaceSettingAlert(context: Context?, title: String?, btnPos: String?,
                                     positiveListener: CreateGameDialogActionListener) {
            val rows = context?.resources?.getStringArray(R.array.rows)
            val columns = context?.resources?.getStringArray(R.array.colums)
            val memorize = context?.resources?.getStringArray(R.array.memorize)
            val recall = context?.resources?.getStringArray(R.array.recall)
//            val dataAdapter = ArrayAdapter<String>(context!!,
//                    android.R.layout.simple_spinner_item, names)
//            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            val builder = android.support.v7.app.AlertDialog.Builder(ContextThemeWrapper(context, R.style.myDialog))
            val dialogView = context?.layoutInflater?.inflate(R.layout.alert_game_face_setting, null)
            builder.setView(dialogView)
            val tvTitle = dialogView?.find<TextView>(R.id.tvTitle)
            val tvRow = dialogView?.find<TextView>(R.id.tvRows)
            val tvTimeDetail = dialogView?.find<TextView>(R.id.tvTimeDetail)
            val seekBar = dialogView?.find<AppCompatSeekBar>(R.id.seekBarRow)
            val spnColumns = dialogView?.find<Spinner>(R.id.spnColumn)
            val spnMemorize = dialogView?.find<Spinner>(R.id.spnMemorize)
            val spnRecall = dialogView?.find<Spinner>(R.id.spnCall)
            val btnPositive = dialogView?.find<Button>(R.id.btnPositive)
            val btnClose = dialogView?.find<ImageView>(R.id.btnClose)
            val dialog = builder.create()
            var memorizeTime = "5"
            var recallTime = "15"
            spnColumns?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, columns)
            spnMemorize?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, memorize)
            spnRecall?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, recall)
            tvTitle?.text = title ?: "Game Setting"
            btnPositive?.text = btnPos ?: "Start Game"
            tvRow?.text = (seekBar?.progress?.plus(3)).toString()
            spnMemorize?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

                override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    when (position) {
                        0 -> {
                            memorizeTime = "5"
                            recallTime = "15"
                        }
                        1 -> {
                            memorizeTime = "15"
                            recallTime = "30"
                        }
                        2 -> {
                            memorizeTime = "15"
                            recallTime = "30"
                        }
                    }
                    tvTimeDetail?.text = """Memorize Time $memorizeTime minutes
                        |Recall Time $recallTime minutes
                    """.trimMargin()
                }

            }
            seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(view: SeekBar?, progress: Int, fromUser: Boolean) {
                    tvRow?.text = (progress.plus(3)).toString()
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })
            btnPositive?.setOnClickListener {
                dialog.dismiss()
                positiveListener.onSettings(
                        rows = (seekBar?.progress?.plus(3)).toString(),
                        columns = spnColumns?.selectedItem.toString(),
                        memorize = memorizeTime,
                        recall = recallTime)
            }
            btnClose?.setOnClickListener {
                dialog.cancel()
            }
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(true)
            dialog.show()
        }

        fun showGameNumberSettingAlert(context: Context?, title: String?, btnPos: String?,
                                     positiveListener: CreateGameDialogActionListener) {
            val rows = context?.resources?.getStringArray(R.array.rows)
            val columns = context?.resources?.getStringArray(R.array.game_number_colums)
            val memorize = context?.resources?.getStringArray(R.array.memorize)
            val recall = context?.resources?.getStringArray(R.array.recall)
//            val dataAdapter = ArrayAdapter<String>(context!!,
//                    android.R.layout.simple_spinner_item, names)
//            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            val builder = android.support.v7.app.AlertDialog.Builder(ContextThemeWrapper(context, R.style.myDialog))
            val dialogView = context?.layoutInflater?.inflate(R.layout.alert_game_number_setting, null)
            builder.setView(dialogView)
            val tvTitle = dialogView?.find<TextView>(R.id.tvTitle)
            val tvRow = dialogView?.find<TextView>(R.id.tvRows)
            val tvTimeDetail = dialogView?.find<TextView>(R.id.tvTimeDetail)
            val seekBar = dialogView?.find<AppCompatSeekBar>(R.id.seekBarRow)
            val spnColumns = dialogView?.find<Spinner>(R.id.spnColumn)
            val spnMemorize = dialogView?.find<Spinner>(R.id.spnMemorize)
            val spnRecall = dialogView?.find<Spinner>(R.id.spnCall)
            val btnPositive = dialogView?.find<Button>(R.id.btnPositive)
            val btnClose = dialogView?.find<ImageView>(R.id.btnClose)
            val dialog = builder.create()
            var memorizeTime = "5"
            var recallTime = "15"
            spnColumns?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, columns)
            spnMemorize?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, memorize)
            spnRecall?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, recall)
            tvTitle?.text = title ?: "Game Setting"
            btnPositive?.text = btnPos ?: "Start Game"
            tvRow?.text = (seekBar?.progress?.plus(3)).toString()
            spnMemorize?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

                override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    when (position) {
                        0 -> {
                            memorizeTime = "5"
                            recallTime = "15"
                        }
                        1 -> {
                            memorizeTime = "15"
                            recallTime = "30"
                        }
                        2 -> {
                            memorizeTime = "15"
                            recallTime = "30"
                        }
                    }
                    tvTimeDetail?.text = """Memorize Time $memorizeTime minutes
                        |Recall Time $recallTime minutes
                    """.trimMargin()
                }

            }
            seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(view: SeekBar?, progress: Int, fromUser: Boolean) {
                    tvRow?.text = (progress.plus(3)).toString()
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })
            btnPositive?.setOnClickListener {
                dialog.dismiss()
                positiveListener.onSettings(
                        rows = (seekBar?.progress?.plus(3)).toString(),
                        columns = spnColumns?.selectedItem.toString(),
                        memorize = memorizeTime,
                        recall = recallTime)
            }
            btnClose?.setOnClickListener {
                dialog.cancel()
            }
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(true)
            dialog.show()
        }

        fun showGameImageSettingAlert(context: Context?, title: String?, btnPos: String?,
                                     positiveListener: CreateGameDialogActionListener) {
            val columns = context?.resources?.getStringArray(R.array.image_colums)
            val memorize = context?.resources?.getStringArray(R.array.memorize)
            val recall = context?.resources?.getStringArray(R.array.recall)
            val builder = android.support.v7.app.AlertDialog.Builder(ContextThemeWrapper(context, R.style.myDialog))
            val dialogView = context?.layoutInflater?.inflate(R.layout.alert_game_face_setting, null)
            builder.setView(dialogView)
            val tvTitle = dialogView?.find<TextView>(R.id.tvTitle)
            val tvRow = dialogView?.find<TextView>(R.id.tvRows)
            val tvTimeDetail = dialogView?.find<TextView>(R.id.tvTimeDetail)
            val seekBar = dialogView?.find<AppCompatSeekBar>(R.id.seekBarRow)
            val spnColumns = dialogView?.find<Spinner>(R.id.spnColumn)
            val spnMemorize = dialogView?.find<Spinner>(R.id.spnMemorize)
            val spnRecall = dialogView?.find<Spinner>(R.id.spnCall)
            val btnPositive = dialogView?.find<Button>(R.id.btnPositive)
            val btnClose = dialogView?.find<ImageView>(R.id.btnClose)
            val dialog = builder.create()
            var memorizeTime = "5"
            var recallTime = "15"
            spnColumns?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, columns)
            spnMemorize?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, memorize)
            spnRecall?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, recall)
            tvTitle?.text = title ?: "Game Setting"
            btnPositive?.text = btnPos ?: "Start Game"
            tvRow?.text = (seekBar?.progress?.plus(3)).toString()
            spnMemorize?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

                override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    when (position) {
                        0 -> {
                            memorizeTime = "5"
                            recallTime = "15"
                        }
                        1 -> {
                            memorizeTime = "15"
                            recallTime = "30"
                        }
                        2 -> {
                            memorizeTime = "15"
                            recallTime = "30"
                        }
                    }
                    tvTimeDetail?.text = """Memorize Time $memorizeTime minutes
                        |Recall Time $recallTime minutes
                    """.trimMargin()
                }

            }
            seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(view: SeekBar?, progress: Int, fromUser: Boolean) {
                    tvRow?.text = (progress.plus(3)).toString()
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })
            btnPositive?.setOnClickListener {
                dialog.dismiss()
                positiveListener.onSettings(
                        rows = (seekBar?.progress?.plus(3)).toString(),
                        columns = spnColumns?.selectedItem.toString(),
                        memorize = memorizeTime,
                        recall = recallTime)
            }
            btnClose?.setOnClickListener {
                dialog.cancel()
            }
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(true)
            dialog.show()
        }

        fun showGameWordSettingAlert(context: Context?, title: String?, btnPos: String?,
                                      positiveListener: CreateGameDialogActionListener) {
            val columns = context?.resources?.getStringArray(R.array.image_colums)
            val memorize = context?.resources?.getStringArray(R.array.memorize)
            val recall = context?.resources?.getStringArray(R.array.recall)
            val builder = android.support.v7.app.AlertDialog.Builder(ContextThemeWrapper(context, R.style.myDialog))
            val dialogView = context?.layoutInflater?.inflate(R.layout.alert_game_face_setting, null)
            builder.setView(dialogView)
            val tvTitle = dialogView?.find<TextView>(R.id.tvTitle)
            val tvRow = dialogView?.find<TextView>(R.id.tvRows)
            val tvTimeDetail = dialogView?.find<TextView>(R.id.tvTimeDetail)
            val seekBar = dialogView?.find<AppCompatSeekBar>(R.id.seekBarRow)
            val spnColumns = dialogView?.find<Spinner>(R.id.spnColumn)
            val spnMemorize = dialogView?.find<Spinner>(R.id.spnMemorize)
            val spnRecall = dialogView?.find<Spinner>(R.id.spnCall)
            val btnPositive = dialogView?.find<Button>(R.id.btnPositive)
            val btnClose = dialogView?.find<ImageView>(R.id.btnClose)
            val dialog = builder.create()
            var memorizeTime = "5"
            var recallTime = "15"
            spnColumns?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, columns)
            spnMemorize?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, memorize)
            spnRecall?.adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, recall)
            tvTitle?.text = title ?: "Game Setting"
            btnPositive?.text = btnPos ?: "Start Game"
            tvRow?.text = (seekBar?.progress?.plus(20)).toString()
            spnMemorize?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

                override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    when (position) {
                        0 -> {
                            memorizeTime = "5"
                            recallTime = "15"
                        }
                        1 -> {
                            memorizeTime = "15"
                            recallTime = "30"
                        }
                        2 -> {
                            memorizeTime = "15"
                            recallTime = "30"
                        }
                    }
                    tvTimeDetail?.text = """Memorize Time $memorizeTime minutes
                        |Recall Time $recallTime minutes
                    """.trimMargin()
                }

            }
            seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(view: SeekBar?, progress: Int, fromUser: Boolean) {
                    tvRow?.text = (progress.plus(20)).toString()
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })
            btnPositive?.setOnClickListener {
                dialog.dismiss()
                positiveListener.onSettings(
                        rows = (seekBar?.progress?.plus(20)).toString(),
                        columns = spnColumns?.selectedItem.toString(),
                        memorize = memorizeTime,
                        recall = recallTime)
            }
            btnClose?.setOnClickListener {
                dialog.cancel()
            }
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(true)
            dialog.show()
        }

        interface MyDialogActionListener {
            fun onPositiveAnswer()

            fun onNegativeAnswer()
        }

        interface CreateGameDialogActionListener {
            fun onSettings(rows: String, columns: String, memorize: String, recall: String)
        }
    }
}


