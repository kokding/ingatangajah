package ingatan.gajah.com.ingatangajah.repository.request

import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.model.Word
import ingatan.gajah.com.ingatangajah.repository.response.GameSettings
import ingatan.gajah.com.ingatangajah.repository.response.Order

data class GameWordAnswerRequest(
        @SerializedName("game_settings")
        val gameSettings: GameSettings,
        @SerializedName("questions")
        val questions: List<Word>,
        @SerializedName("answers")
        val answers: List<Word>
)
