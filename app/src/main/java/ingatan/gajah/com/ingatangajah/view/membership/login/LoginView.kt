package ingatan.gajah.com.ingatangajah.view.membership.login

import ingatan.gajah.com.ingatangajah.base.IBaseView

interface LoginView {
    fun goToMainActivity()
    fun error(message: String?)
}