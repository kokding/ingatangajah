package ingatan.gajah.com.ingatangajah.repository.response

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Order (

        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("order")
        val order: Int,
        @Expose
        @SerializedName("image")
        val image: String) : Parcelable
