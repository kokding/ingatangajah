package ingatan.gajah.com.ingatangajah.view.game.face

import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse

interface GameFaceView{
    fun goToResult(body: GameFaceResultResponse?)
    fun error(message: String?)

}