package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.utils.setImage
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_game_order.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.sdk25.coroutines.textChangedListener


class GameOrderAdapter(private val context: Context?, private val data: List<GameOrder>)
    : RecyclerView.Adapter<GameOrderAdapter.GameOrderViewHolder>() {

    lateinit var listener: (GameOrder) -> Unit

    var state: Int = AppConstant.STATE_MEMORIZE

    var isHide = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameOrderViewHolder = GameOrderViewHolder(LayoutInflater.from(context).inflate(R.layout.item_game_order, parent, false))

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int = position

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: GameOrderViewHolder, position: Int) = holder.bindItem(data[position], listener, state, isHide)

    class GameOrderViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: GameOrder, listener: (GameOrder) -> Unit, state: Int, hide: Boolean) {
            tvAnswer.visibility = if (state == AppConstant.STATE_MEMORIZE) View.VISIBLE else View.GONE
            edtGame.visibility = if (state == AppConstant.STATE_RECALL) View.VISIBLE else View.GONE
            setImage(containerView, item.img, pb_img, containerView.resources.getDrawable(R.drawable.icons_ingatan_gajah), img_game)
            when (hide) {
                true -> {
                    tvAnswer.text = ""
                    tvAnswer.backgroundColor = containerView.resources.getColor(R.color.grey_dot)
                }
                false -> {
                    tvAnswer.text = item.question
                    tvAnswer.backgroundColor = containerView.resources.getColor(R.color.real_white)
                }
            }
            edtGame.textChangedListener {
                afterTextChanged {
                    item.answer = it?.toString()
                    edtGame.background = containerView.resources.getDrawable(
                            if (it.isNullOrEmpty()) R.drawable.edittext_empty_selector
                            else R.drawable.edittext_answered_selector)
                }
            }
            img_game.setOnClickListener{
                setImage(containerView, item.img, pb_img, containerView.resources.getDrawable(R.drawable.icons_ingatan_gajah), img_game)
            }
            containerView.setOnClickListener { listener(item) }
        }
    }
}