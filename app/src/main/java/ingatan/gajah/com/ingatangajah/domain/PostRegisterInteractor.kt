package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.RegisterRequest
import ingatan.gajah.com.ingatangajah.repository.response.ResponseModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostRegisterInteractor() {
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postRegister(request: RegisterRequest, token: String): Observable<Response<ResponseModel>> {

        return apiManager
                .register(request, "Bearer " + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}