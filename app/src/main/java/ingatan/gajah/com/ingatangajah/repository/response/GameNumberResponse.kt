package ingatan.gajah.com.ingatangajah.repository.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameNumberResponse(
        @SerializedName("code")
        val code: Int,
        @SerializedName("status")
        val status: String,
        @SerializedName("game_settings")
        var gameSettings: GameSettings,
        @SerializedName("data")
        val data: List<Int>
) : Parcelable
