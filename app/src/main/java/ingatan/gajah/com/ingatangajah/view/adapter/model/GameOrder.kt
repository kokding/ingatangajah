package ingatan.gajah.com.ingatangajah.view.adapter.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameOrder (
        var id: Int,
        var img: String,
        var answer: String?,
        var question: String?
) : Parcelable
