package ingatan.gajah.com.ingatangajah.repository.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GameSettings(
        @Expose
        @SerializedName("rows")
        val rows: Int,
        @Expose
        @SerializedName("columns")
        val columns: Int,
        @Expose
        @SerializedName("highlight")
        val highlight: Int,
        @Expose
        @SerializedName("recall_time")
        val recallTime: Int,
        @Expose
        @SerializedName("memorize_time")
        val memorizeTime: Int,
        @Expose
        @SerializedName("user_recall_time")
        val userRecallTime: Int,
        @Expose
        @SerializedName("user_memorize_time")
        val userMemorizeTime: Int)