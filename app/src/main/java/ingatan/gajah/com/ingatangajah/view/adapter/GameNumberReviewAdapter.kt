package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.AnswerResult
import ingatan.gajah.com.ingatangajah.repository.model.GameNumberAnswerResult
import ingatan.gajah.com.ingatangajah.utils.setImage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_face_review.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor


class GameNumberReviewAdapter(private val context: Context, private val data: List<GameNumberAnswerResult>)
    : RecyclerView.Adapter<GameNumberReviewAdapter.GameNumberAnswerResultViewHolder>() {

    lateinit var listener: (GameNumberAnswerResult) -> Unit

    var state = AppConstant.STATE_ANSWER

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameNumberAnswerResultViewHolder = GameNumberAnswerResultViewHolder(LayoutInflater.from(context).inflate(R.layout.item_game_number_answer, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: GameNumberAnswerResultViewHolder, position: Int) = holder.bindItem(data[position], listener, state)

    class GameNumberAnswerResultViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: GameNumberAnswerResult, listener: (GameNumberAnswerResult) -> Unit, state: Int) {
            var showAnswer = true
            //show answer when review opened
            when(state){
                AppConstant.STATE_ANSWER -> showAnswer(item)
                AppConstant.STATE_QUESTION -> showQuestion(item)
            }
//            showAnswer(item)
            tvAnswer.setOnClickListener {
                showAnswer = !showAnswer
                when (showAnswer) {
                    true -> {
                        showAnswer(item)
                    }
                    false -> {
                        showQuestion(item)
                    }
                }
            }
        }

        private fun showQuestion(item: GameNumberAnswerResult) {
            tvAnswer.text = item.question.toString()
            tvAnswer.backgroundColor = containerView.resources.getColor(R.color.grey_dot)
            tvAnswer.textColor = containerView.resources.getColor(R.color.semi_black)
        }

        fun showAnswer(item: GameNumberAnswerResult) {
            tvAnswer.backgroundColor = containerView.resources.getColor(R.color.real_white)
            tvAnswer.text = if (item.answer.toString().equals("-1")) "" else item.answer.toString()
            when (item.answer.toString()) {
                "-1" -> tvAnswer.textColor = containerView.resources.getColor(R.color.grey_dot)
                else -> {
                    if (item.answer.toString().equals(item.question.toString()))
                        tvAnswer.textColor = containerView.resources.getColor(R.color.slime_green)
                    else
                        tvAnswer.textColor = containerView.resources.getColor(R.color.tomato_red)
                }
            }
        }
    }
}