package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.model.History

data class HistoryResponse(

        @SerializedName("code")
        val code: Int,
        @Expose
        @SerializedName("status")
        val status: String,
        @Expose
        @SerializedName("meta")
        val meta: Meta,
        @SerializedName("data")
        val histories: List<History>)