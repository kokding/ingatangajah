package ingatan.gajah.com.ingatangajah.view.membership.register

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.PostRegisterInteractor
import ingatan.gajah.com.ingatangajah.repository.request.RegisterRequest
import ingatan.gajah.com.ingatangajah.repository.response.ResponseModel
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver
import retrofit2.Response

class RegisterPresenter(val view: RegisterView, val disposableManager: DisposableManager, val context: Context) {

    private val postRegisterInteractor: PostRegisterInteractor = PostRegisterInteractor()
    private val preferenceManager: PreferenceManager = PreferenceManager(context)
    fun postRegister(fullname: String, username: String, birthdate: String, email: String, password: String, passwordConfirmation: String) {
        val request: RegisterRequest
        request = RegisterRequest(fullname, username, birthdate, email, password, passwordConfirmation)

        val dispose = postRegisterInteractor.postRegister(request, preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<ResponseModel>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<ResponseModel>) {
                when (t.body()?.code) {
                    400, 401, 404, 204 -> view.error(t.body()?.message)
                    422 -> view.handleInvalidData(t.body()?.data)
                    else -> {
                        view.goToLogin()
                    }
                }
            }

            override fun onError(e: Throwable) {
            }

        })
        disposableManager.add(dispose)
    }

}