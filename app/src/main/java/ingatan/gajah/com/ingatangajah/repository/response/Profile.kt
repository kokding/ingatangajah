package ingatan.gajah.com.ingatangajah.repository.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Profile(
        
        @SerializedName("id")
        val userId: String?,

        @SerializedName("email")
        val email: String?,

        @SerializedName("username")
        val username: String?,

        @SerializedName("fullName")
        val fullname: String?,

        @SerializedName("birth_of_date")
        val birthOfDate: String?,

        @SerializedName("avatar_url")
        var avatar: String?,

        @SerializedName("rank")
        val rank: Int?) : Parcelable