package ingatan.gajah.com.ingatangajah.view.game.word

import android.support.v4.view.ViewPager
import android.view.MenuItem
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.AnswerResultWord
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResultResponse
import ingatan.gajah.com.ingatangajah.view.adapter.viewpager.WordReviewViewPagerAdapter
import kotlinx.android.synthetic.main.activity_game_word_review.*

class GameWordReviewActivity : BaseActivity() {

    private lateinit var vpAdapter: WordReviewViewPagerAdapter
    private lateinit var data: GameWordResultResponse
    private var answers: MutableList<AnswerResultWord> = mutableListOf()
    private var showAnswer = true

    companion object {
        var listener: WordShowAnswerListener? = null

        fun setWordShowAnswerListener(listener: WordShowAnswerListener) {
            this.listener = listener
        }
    }

    override fun getLayoutResource(): Int = R.layout.activity_game_word_review

    override fun initIntent() {
        data = intent.getParcelableExtra(AppConstant.KEY_REVIEW_DATA)
        answers.clear()
        data.data.forEach { answers.add(it) }
    }

    override fun initUI() {
        vpAdapter = WordReviewViewPagerAdapter(supportFragmentManager, data,answers,1, data.gameSettings.columns)
        vpWord.adapter = vpAdapter
        vpWord.offscreenPageLimit = 0

        tvCounter.text = "1/${data.gameSettings.columns}"
        vpWord.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{

            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                tvCounter.text = "${position+1}/${data.gameSettings.columns}"
            }
        })
    }

    override fun initAction() {
        btnBack.setOnClickListener{
            onBackPressed()
        }
//        btnShowAnswer.setOnClickListener {
//            showAnswer = !showAnswer
//            btnShowAnswer.text = if (!showAnswer) "Show Answer" else "Show Question"
//            listener?.onShowButtonClicked()
//        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    interface WordShowAnswerListener {

        fun onShowButtonClicked()
    }
}
