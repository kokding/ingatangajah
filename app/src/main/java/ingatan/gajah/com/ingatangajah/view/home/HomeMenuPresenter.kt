package ingatan.gajah.com.ingatangajah.view.home

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.GetPersonalLeaderboardInteractor
import ingatan.gajah.com.ingatangajah.domain.IGetPointInteractor
import ingatan.gajah.com.ingatangajah.repository.model.Schedule
import ingatan.gajah.com.ingatangajah.repository.response.PersonalLeaderboardRsponse
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import retrofit2.Response

class HomeMenuPresenter(val view: HomeMenuView,val disposable: DisposableManager, context: Context?) : AnkoLogger, IGetPointInteractor {

    val getPersonalLeaderboardInteractor: GetPersonalLeaderboardInteractor
            = GetPersonalLeaderboardInteractor()

    private val preferenceManager = PreferenceManager(context!!)

    override fun onResponse(schedules: List<Schedule>) {
        info { "nih $schedules" }
    }

    override fun onFailed(error: Throwable?) {
        info { "error nih" }
    }

    override fun getUserCredential() {
    }

    fun getLeaderboard(gameId: Int?) {
        val dispose = getPersonalLeaderboardInteractor.getLeaderboard(gameId, "Bearer " + preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<PersonalLeaderboardRsponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<PersonalLeaderboardRsponse>) {
//                view.hideLoading()
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.showDataPersonal(t.body()?.personalLeaderboard, preferenceManager.getUserCredential())
                    }
                }
            }

            override fun onError(e: Throwable) {
//                view.hideLoading()
                view.error(e.message)
            }
        })
        disposable.add(dispose)
    }
}