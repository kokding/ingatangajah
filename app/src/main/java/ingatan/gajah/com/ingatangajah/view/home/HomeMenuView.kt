package ingatan.gajah.com.ingatangajah.view.home

import ingatan.gajah.com.ingatangajah.repository.model.Schedule
import ingatan.gajah.com.ingatangajah.repository.response.Data
import ingatan.gajah.com.ingatangajah.repository.response.PersonalLeaderboard

interface HomeMenuView {
    fun showData(schedules: List<Schedule>)
    fun userData(userCredential: Data)
    fun error(message: String?)
    fun showDataPersonal(personalLeaderboard: PersonalLeaderboard?, userCredential: Data?)
}