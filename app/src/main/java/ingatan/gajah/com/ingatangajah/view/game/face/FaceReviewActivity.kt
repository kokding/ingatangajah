package ingatan.gajah.com.ingatangajah.view.game.face

import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.AnswerResult
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse
import ingatan.gajah.com.ingatangajah.view.adapter.FaceReviewAdapter
import kotlinx.android.synthetic.main.activity_face_review.*

class FaceReviewActivity : BaseActivity() {

    private lateinit var data: GameFaceResultResponse
    private lateinit var adapter: FaceReviewAdapter
    private var answers: MutableList<AnswerResult> = mutableListOf()
    private var showAnswer = true

    override fun getLayoutResource(): Int = R.layout.activity_face_review

    override fun initIntent() {
        data = intent.getParcelableExtra(AppConstant.KEY_REVIEW_DATA)
        adapter = FaceReviewAdapter(this@FaceReviewActivity, answers)
        adapter.setHasStableIds(true)
        adapter.state = AppConstant.STATE_ANSWER
        rvReview.layoutManager = GridLayoutManager(this@FaceReviewActivity, data.gameSettings.columns)
        rvReview.adapter = adapter

        answers.addAll(data.data)
    }

    override fun initUI() {

    }

    override fun initAction() {
        btnShowAnswer.setOnClickListener {
            showAnswer = !showAnswer
            adapter.state = if (showAnswer) AppConstant.STATE_ANSWER else AppConstant.STATE_QUESTION
            btnShowAnswer.text = if (!showAnswer) "Show Answer" else "Show Question"
            adapter.notifyDataSetChanged()
        }

        adapter.listener = {

        }

        btnBack.setOnClickListener{
            onBackPressed()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
