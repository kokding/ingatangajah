package ingatan.gajah.com.ingatangajah.view.game.order

import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.AnswerResultImage
import ingatan.gajah.com.ingatangajah.repository.response.GameImageResultResponse
import ingatan.gajah.com.ingatangajah.view.adapter.GameImageReviewAdapter
import kotlinx.android.synthetic.main.activity_game_order_review.*

class GameOrderReviewActivity : BaseActivity() {

    private lateinit var data: GameImageResultResponse
    private lateinit var adapter: GameImageReviewAdapter
    private var answers: MutableList<AnswerResultImage> = mutableListOf()
    private var showAnswer = true

    override fun getLayoutResource(): Int = R.layout.activity_game_order_review

    override fun initIntent() {
        data = intent.getParcelableExtra(AppConstant.KEY_REVIEW_DATA)
        adapter = GameImageReviewAdapter(this@GameOrderReviewActivity, answers)
        adapter.setHasStableIds(true)
        adapter.state = AppConstant.STATE_ANSWER
        rvReview.layoutManager = GridLayoutManager(this@GameOrderReviewActivity, data.gameSettings.columns)
        rvReview.adapter = adapter

        answers.addAll(data.data)
    }

    override fun initUI() {
    }

    override fun initAction() {
        btnShowAnswer.setOnClickListener {
            showAnswer = !showAnswer
            adapter.state = if (showAnswer) AppConstant.STATE_ANSWER else AppConstant.STATE_QUESTION
            btnShowAnswer.text = if (!showAnswer) "Show Answer" else "Show Question"
            adapter.notifyDataSetChanged()
        }
        adapter.listener = {

        }

        btnBack.setOnClickListener{
            onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
