package ingatan.gajah.com.ingatangajah.view.game.face.faceanswer

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.RecyclerView
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.Answer
import ingatan.gajah.com.ingatangajah.repository.request.GameFaceAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse
import ingatan.gajah.com.ingatangajah.utils.MessageFactory
import ingatan.gajah.com.ingatangajah.utils.gone
import ingatan.gajah.com.ingatangajah.utils.hideSoftKeyboard
import ingatan.gajah.com.ingatangajah.utils.visible
import ingatan.gajah.com.ingatangajah.view.adapter.FaceNavigationAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.GameFaceAnswerAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import ingatan.gajah.com.ingatangajah.view.game.face.*
import kotlinx.android.synthetic.main.fragment_game_face_answer.*
import kotlinx.android.synthetic.main.layout_face_navigation.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.info
import org.jetbrains.anko.sdk25.coroutines.textChangedListener
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast


class GameFaceAnswerFragment : BaseFragment(), GameFaceView, GameFaceActivity.GameEndListener, AnkoLogger {

    var gameOrder: MutableList<GameOrder> = mutableListOf()
    var answers: MutableList<Answer> = mutableListOf()
    private val disposableManager = DisposableManager()
    lateinit var adapter: GameFaceAnswerAdapter
    lateinit var navigationAdapter: FaceNavigationAdapter
    private lateinit var presenter: GameFacePresenter
    lateinit var data: GameFaceResponse
    private var state: Int = AppConstant.STATE_MEMORIZE
    private var lastScrollPosition: Int = 0

    companion object {

        fun newInstance(category: GameFaceResponse, state: Int) = GameFaceAnswerFragment().apply {
            arguments = bundleOf(
                    AppConstant.KEY_GAME_FACE to category,
                    AppConstant.KEY_GAME_STATE to state)
        }
    }

    override fun getLayoutResource(): Int = R.layout.fragment_game_face_answer

    override fun initLib() {
        presenter = GameFacePresenter(this, disposableManager, context!!)
        GameFaceActivity.setGameListener(this)
    }

    override fun initIntent() {
        data = arguments?.getParcelable(AppConstant.KEY_GAME_FACE)!!
        state = arguments?.getInt(AppConstant.KEY_GAME_STATE, AppConstant.STATE_MEMORIZE)!!
    }

    override fun initUI() {
        adapter = GameFaceAnswerAdapter(context, gameOrder)
        adapter.setHasStableIds(true)
        adapter.state = state
        val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvQuestion.layoutManager = linearLayoutManager
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(rvQuestion)
        rvQuestion.adapter = adapter
        rvQuestion.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                lastScrollPosition = linearLayoutManager.findLastVisibleItemPosition()
                hideSoftKeyboard(edtGame, context)
                edtGame.setText(gameOrder[lastScrollPosition].answer.toString())
                edtGame.background = resources.getDrawable(
                        if (gameOrder[lastScrollPosition].answer.isNullOrEmpty()) R.drawable.edittext_empty_selector
                        else R.drawable.edittext_answered_selector)
                tvCounter.setText("${lastScrollPosition + 1}/${gameOrder.size}")
            }
        })

        navigationAdapter = FaceNavigationAdapter(context, gameOrder)
        navigationAdapter.setHasStableIds(true)
        rvNavigation.layoutManager = GridLayoutManager(context, 5)
        rvNavigation.adapter = navigationAdapter
        iniData()
    }

    private fun iniData() {
        data.data.forEach {
            gameOrder.add(GameOrder(
                    id = it.id,
                    img = it.image,
                    question = it.fullname,
                    answer = ""))
        }

//        adapter.notifyDataSetChanged()
        for (i in 0 until adapter.getItemCount()) {
            // update each item itself
            adapter.notifyItemChanged(i, gameOrder.get(i))
            navigationAdapter.notifyItemChanged(i, gameOrder.get(i))
        }
    }

    override fun initAction() {
        adapter.listener = {

        }

        navigationAdapter.listener = { it, position ->
            rvQuestion.scrollToPosition(position)
            containerNavigation.gone()
        }

        btnLeft.setOnClickListener {
            if (lastScrollPosition < 1) rvQuestion.scrollToPosition(gameOrder.size - 1)
            else rvQuestion.scrollToPosition(lastScrollPosition - 1)
        }

        btnRight.setOnClickListener {
            if (lastScrollPosition < (gameOrder.size - 1)) rvQuestion.scrollToPosition(lastScrollPosition + 1)
            else rvQuestion.scrollToPosition(0)
        }

        edtGame.textChangedListener {
            afterTextChanged {
                gameOrder[lastScrollPosition].answer = it?.toString()
                edtGame.background = resources.getDrawable(
                        if (it.isNullOrEmpty()) R.drawable.edittext_empty_selector
                        else R.drawable.edittext_answered_selector)
                adapter.notifyDataSetChanged()
                navigationAdapter.notifyDataSetChanged()
            }
        }

        btnGrid.setOnClickListener {
            containerNavigation.visible()
        }

        btnClose.setOnClickListener {
            containerNavigation.gone()
        }
    }

    override fun initProcess() {
    }

    override fun goToResult(body: GameFaceResultResponse?) {
        startActivity<FaceResultActivity>(AppConstant.KEY_RESULT to body)
        activity?.finish()
    }

    override fun onGameEnd(userMemorizeTime: Long, userRecallTime: Long) {
        MessageFactory.showDoneAlert(context,
                "Game End",
                "Let seeing your result",
                "Go to result",
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {
//                        toast(gameOrder.toString())
                        gameOrder.forEach {
                            answers.add(Answer(it.id,
                                    if (it.answer.isNullOrBlank()) "-1"
                                    else it.answer ?: "-1"))
                        }
                        data.gameSettings.userMemorize = userMemorizeTime.toInt()
                        data.gameSettings.userRecall = userRecallTime.toInt()
                        val answer = GameFaceAnswerRequest(data.gameSettings, data.data, answers)
                        info("MANTAP " + answer.toString())
                        presenter.postAnswer(answer)
                    }

                    override fun onNegativeAnswer() {
                    }
                })
    }

    override fun onPause() {
        super.onPause()
        disposableManager.dispose()
    }

    override fun onVisibleMemorizeClicked(isHide: Boolean) {
    }

    override fun error(message: String?) {
        toast(message?:"Terjadi Kesalahan, Coba Beberapa saat lagi")
    }
}
