package ingatan.gajah.com.ingatangajah.repository.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class History (

        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("created_by")
        val createdBy: String,
        @Expose
        @SerializedName("game_id")
        val gameId: Int,
        @Expose
        @SerializedName("game_settings")
        val gameSettings: GameSettings,
        @Expose
        @SerializedName("total_question")
        val totalQuestion: Int,
        @Expose
        @SerializedName("correct")
        val correct: Int,
        @Expose
        @SerializedName("incorrect")
        val incorrect: Int,
        @Expose
        @SerializedName("points")
        val points: Int,
        @Expose
        @SerializedName("created_at")
        val createdAt: String)
