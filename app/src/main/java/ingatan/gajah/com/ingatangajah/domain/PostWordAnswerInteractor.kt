package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.GameFaceAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.request.GameWordAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResultResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostWordAnswerInteractor{
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postWordAnswer(gameId:Int, answer: GameWordAnswerRequest, token: String): Observable<Response<GameWordResultResponse>> {

        return apiManager
                .postGameWordAnswer(
                        gameId = gameId,
                        loginToken = "Bearer "+token,
                        answerRequest = answer
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}