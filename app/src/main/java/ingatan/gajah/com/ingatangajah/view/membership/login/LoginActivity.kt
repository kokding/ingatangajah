package ingatan.gajah.com.ingatangajah.view.membership.login

import android.support.design.widget.TextInputLayout
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.utils.*
import ingatan.gajah.com.ingatangajah.view.main.MainActivity
import ingatan.gajah.com.ingatangajah.view.membership.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginActivity : BaseActivity(), LoginView {

    private lateinit var presenter: LoginPresenter
    private lateinit var disposableManager: DisposableManager
    private lateinit var field: Array<TextInputLayout>

    fun start() {
        startActivity<LoginActivity>()
    }

    override fun initAction() {
        btn_login.setOnClickListener {
            if (validate(field)!!) {
                presenter.postLogin(til_email.editText?.text.toString(), til_password.editText?.text.toString())
            }
        }
        tv_forgot_password.setOnClickListener {
            MessageFactory.showDoneAlert(this@LoginActivity, "Berhasil", """tes tes tes""", "test",
                    object : MessageFactory.Companion.MyDialogActionListener {
                override fun onPositiveAnswer() {
                    toast("tess")
                }

                override fun onNegativeAnswer() {
                }

            })
        }

        tv_register.setOnClickListener { startActivity<RegisterActivity>() }
    }

    override fun initUI() {
        field = arrayOf(til_email,til_password)
        editTextWatcher(field)
        tv_forgot_password.invisible()
    }

    override fun initIntent() {
        disposableManager = DisposableManager()
        presenter = LoginPresenter(this, disposableManager, this@LoginActivity)

    }

    override fun getLayoutResource(): Int = R.layout.activity_login

    override fun goToMainActivity() {
        startActivity<MainActivity>()
        finish()
    }

    override fun error(message: String?) {
        toast(message ?: "Terjadi kesalahan. Coba beberapa saat lagi")
    }


}
