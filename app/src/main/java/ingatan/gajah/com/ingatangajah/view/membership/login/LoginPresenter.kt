package ingatan.gajah.com.ingatangajah.view.membership.login

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.PostLoginInteractor
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.request.LoginRequest
import ingatan.gajah.com.ingatangajah.repository.response.LoginResponse
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver
import org.jetbrains.anko.AnkoLogger
import retrofit2.Response

class LoginPresenter(val view: LoginView, val disposableManager: DisposableManager, loginActivity: Context) : AnkoLogger {

    private val loginInteractor: PostLoginInteractor = PostLoginInteractor()
    private val preferenceManager: PreferenceManager = PreferenceManager(loginActivity)
    fun postLogin(email: String, password: String) {
        val request = LoginRequest(email, password)
        val dispose = loginInteractor.postLogin(request, preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<LoginResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: retrofit2.Response<LoginResponse>) {
                when (t.body()?.responseCode) {
                    400, 401, 404, 204 -> view.error(t.body()?.message)
                    else -> {
                        preferenceManager.setAnonymousToken(t.headers().get(AppConstant.LOGIN_TOKEN_HEADER)
                                ?: "")
                        preferenceManager.setUserCredential(t.body()?.data!!)
                        view.goToMainActivity()
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }


}