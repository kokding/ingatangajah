package ingatan.gajah.com.ingatangajah.view.game.word

import android.support.v7.widget.LinearLayoutManager
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResponse
import ingatan.gajah.com.ingatangajah.view.adapter.GameWordAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import kotlinx.android.synthetic.main.fragment_word.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.info

class WordFragment : BaseFragment(), AnkoLogger {

    private lateinit var adapter : GameWordAdapter
    var gameQuestion: MutableList<GameOrder> = mutableListOf()
    private var state: Int = AppConstant.STATE_MEMORIZE
    private var columns: Int = 0
    private lateinit var data: GameWordResponse
    companion object {

        fun newInstance(category: GameWordResponse, state: Int, coulmn: Int, gameQuestion: List<GameOrder>) = WordFragment().apply {
            arguments = bundleOf(
                    AppConstant.KEY_GAME_WORDS to category,
                    AppConstant.KEY_POSITION to coulmn,
                    AppConstant.KEY_GAME_STATE to state,
                    AppConstant.KEY_GAME_QUESTION to gameQuestion)
        }
    }
    override fun getLayoutResource(): Int = R.layout.fragment_word

    override fun initLib() {

    }

    override fun initIntent() {
        state = arguments?.getInt(AppConstant.KEY_GAME_STATE, AppConstant.STATE_MEMORIZE)!!
        columns = arguments?.getInt(AppConstant.KEY_POSITION, 0)!!
        data = arguments?.getParcelable(AppConstant.KEY_GAME_WORDS)!!
        gameQuestion = arguments?.getParcelableArrayList(AppConstant.KEY_GAME_QUESTION)!!
        info{"word state $state"}
    }

    override fun initUI() {
        adapter = GameWordAdapter(context, gameQuestion, columns, data.gameSettings.rows)
        adapter.setHasStableIds(true)
        adapter.state = state
        rvWord.layoutManager = LinearLayoutManager(context)
        rvWord.adapter = adapter
//        data.data.forEach {
//            gameQuestion.add(GameOrder(
//                    id = it.id,
//                    img = "",
//                    question = it.name,
//                    answer = ""))
//        }

        adapter.notifyDataSetChanged()
    }

    override fun initAction() {
        adapter.listener= {}
    }

    override fun initProcess() {

    }

}
