package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PersonalLeaderboard(
        @Expose
        @SerializedName("rank")
        val rank: String,
        @Expose
        @SerializedName("points")
        val points: String)