package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.GameImageAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameImageResultResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostGameImageAnswerInteractor{
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postGameImageAnswer(gameId:Int, answer: GameImageAnswerRequest, token: String): Observable<Response<GameImageResultResponse>> {

        return apiManager
                .postGameImageAnswer(
                        gameId = gameId,
                        loginToken = "Bearer "+token,
                        answerRequest = answer
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}