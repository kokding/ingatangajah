package ingatan.gajah.com.ingatangajah.repository.response

import ingatan.gajah.com.ingatangajah.repository.model.FieldEmpty

data class ResponseModel(
        val code: Int,
        val status: String,
        val message:String?,
        val data: List<FieldEmpty>?
)
