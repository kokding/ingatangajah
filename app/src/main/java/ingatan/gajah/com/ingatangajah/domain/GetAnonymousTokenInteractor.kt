package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.response.ResponseModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Response
import okhttp3.ResponseBody

class GetAnonymousTokenInteractor(){
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }
    fun getAnonymousToken(): Observable<retrofit2.Response<ResponseModel>> {

        return apiManager
                .getAnonymousToken("DCI7kIPo89KrE3ZI6esYIAj4Y9iN8Ec8")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}