package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.RandomRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostRandomFaceQuestionInteractor{

    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postRandomFaceQuestion(game: Int, token: String, request: RandomRequest): Observable<Response<GameFaceResponse>> {

        return apiManager
                .getRandomFaceQuestion(
                        gameId = game,
                        loginToken = token,
                        question = request
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}