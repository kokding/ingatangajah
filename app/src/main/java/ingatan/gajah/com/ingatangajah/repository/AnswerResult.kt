package ingatan.gajah.com.ingatangajah.repository

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.model.Answer
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AnswerResult(

        @SerializedName("id")
        val id: Int,

        @SerializedName("question")
        val question: Question,

        @SerializedName("answer")
        val answer: Answer,

        @SerializedName("status")
        val status: Int) : Parcelable
