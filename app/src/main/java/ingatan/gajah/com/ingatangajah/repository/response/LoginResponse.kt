package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(

        @SerializedName("code")
        val responseCode: Int?,
        @SerializedName("status")
        val responseStatus: String?,
        @SerializedName("message")
        val message: String?,
        @SerializedName("data")
        var data: Data?)
