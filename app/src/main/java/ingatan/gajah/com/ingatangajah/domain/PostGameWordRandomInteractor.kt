package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.RandomRequest
import ingatan.gajah.com.ingatangajah.repository.request.WordRandomRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostGameWordRandomInteractor{

    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postRandomWordQuestion(game: Int, token: String, request: WordRandomRequest): Observable<Response<GameWordResponse>> {

        return apiManager
                .getGameWordRandomQuestion(
                        gameId = game,
                        loginToken = token,
                        question = request
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}