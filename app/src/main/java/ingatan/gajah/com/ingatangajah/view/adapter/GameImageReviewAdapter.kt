package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.AnswerResult
import ingatan.gajah.com.ingatangajah.repository.model.AnswerResultImage
import ingatan.gajah.com.ingatangajah.utils.setImage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_face_review.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor


class GameImageReviewAdapter(private val context: Context, private val data: List<AnswerResultImage>)
    : RecyclerView.Adapter<GameImageReviewAdapter.AnswerResultImageViewHolder>() {

    lateinit var listener: (AnswerResultImage) -> Unit

    var state = AppConstant.STATE_ANSWER

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswerResultImageViewHolder = AnswerResultImageViewHolder(LayoutInflater.from(context).inflate(R.layout.item_game_image_review, parent, false))

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int = position

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun onBindViewHolder(holder: AnswerResultImageViewHolder, position: Int) = holder.bindItem(data[position], listener, state)

    class AnswerResultImageViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: AnswerResultImage, listener: (AnswerResultImage) -> Unit, state: Int) {
            var showAnswer = true
            //set image
            setImage(containerView, item.question.image, pb_img, img_game)
            //show answer when review opened
            when(state){
                AppConstant.STATE_ANSWER -> showAnswer(item)
                AppConstant.STATE_QUESTION -> showQuestion(item)
            }
//            showAnswer(item)
            tvAnswer.setOnClickListener {
                showAnswer = !showAnswer
                when (showAnswer) {
                    true -> {
                        showAnswer(item)
                    }
                    false -> {
                        showQuestion(item)
                    }
                }
            }
        }

        private fun showQuestion(item: AnswerResultImage) {
            tvAnswer.text = item.question.order.toString()
            tvAnswer.backgroundColor = containerView.resources.getColor(R.color.grey_dot)
            tvAnswer.textColor = containerView.resources.getColor(R.color.semi_black)
        }

        fun showAnswer(item: AnswerResultImage) {
            tvAnswer.backgroundColor = containerView.resources.getColor(R.color.real_white)
            tvAnswer.text = if (item.answer.order == -1) "" else item.answer.order.toString()
            when (item.answer.order) {
                -1 -> tvAnswer.textColor = containerView.resources.getColor(R.color.grey_dot)
                else -> {
                    if (item.answer.order == item.question.order)
                        tvAnswer.textColor = containerView.resources.getColor(R.color.slime_green)
                    else
                        tvAnswer.textColor = containerView.resources.getColor(R.color.tomato_red)
                }
            }
        }
    }
}