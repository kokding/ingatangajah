package ingatan.gajah.com.ingatangajah.view.game.order

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.PostFaceAnswerInteractor
import ingatan.gajah.com.ingatangajah.domain.PostGameImageAnswerInteractor
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.request.GameFaceAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.request.GameImageAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameImageResultResponse
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import retrofit2.Response

class GameOrderPresenter(val view: GameOrderView,val disposable: DisposableManager, context: Context) : AnkoLogger {
    val preferenceManager: PreferenceManager = PreferenceManager(context)
    val postGameImageAnswerInteractor: PostGameImageAnswerInteractor = PostGameImageAnswerInteractor()

    fun postAnswer(answer: GameImageAnswerRequest) {
        val dispose = postGameImageAnswerInteractor.postGameImageAnswer(AppConstant.GAME_IMAGE,
                answer, preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<GameImageResultResponse>>(){
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameImageResultResponse>) {
                info("Result"+t.body().toString())
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.goToResult(t.body())
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposable.add(dispose)
    }

}