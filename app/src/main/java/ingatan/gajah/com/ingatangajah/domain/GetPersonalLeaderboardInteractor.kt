package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.response.LeaderboardResponse
import ingatan.gajah.com.ingatangajah.repository.response.PersonalLeaderboardRsponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class GetPersonalLeaderboardInteractor {
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun getLeaderboard(gameId: Int?, token: String): Observable<Response<PersonalLeaderboardRsponse>> {

        return apiManager
                .getPersonalLeaderboard(token, gameId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}