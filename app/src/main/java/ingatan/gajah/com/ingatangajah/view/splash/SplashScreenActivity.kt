package ingatan.gajah.com.ingatangajah.view.splash

import android.os.Handler
import ingatan.gajah.com.ingatangajah.BuildConfig
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.view.main.MainActivity
import ingatan.gajah.com.ingatangajah.view.membership.login.LoginActivity
import kotlinx.android.synthetic.main.activity_splash_screen.*
import org.jetbrains.anko.startActivity

class SplashScreenActivity : BaseActivity(), SplashView {
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable
    private lateinit var presenter: SplashPresenter
    private var disposableManager: DisposableManager? = null

    override fun initAction() {
        presenter = SplashPresenter(this, disposableManager, this@SplashScreenActivity)
        mHandler = Handler()
        mRunnable = Runnable {
            presenter.getAnonymousToken()
        }
        mHandler.postDelayed(
                mRunnable, 2000
        )
    }

    override fun initUI() {
        tvVersion.text = "v"+BuildConfig.VERSION_NAME
    }

    override fun initIntent() {
    }

    override fun getLayoutResource(): Int = R.layout.activity_splash_screen

    override fun goToLogin(header: String?) {
        startActivity<LoginActivity>()
        finish()
    }

    override fun goToMainActivity() {
        startActivity<MainActivity>()
        finish()
    }
}
