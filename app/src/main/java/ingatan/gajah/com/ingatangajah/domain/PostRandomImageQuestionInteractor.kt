package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.ImageRandomRequest
import ingatan.gajah.com.ingatangajah.repository.request.RandomRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameImageResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostRandomImageQuestionInteractor{
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postRandomImageQuestion(game: Int, token: String, request: ImageRandomRequest): Observable<Response<GameImageResponse>> {

        return apiManager
                .getGameImageRandomQuestion(
                        gameId = game,
                        loginToken = token,
                        question = request
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}