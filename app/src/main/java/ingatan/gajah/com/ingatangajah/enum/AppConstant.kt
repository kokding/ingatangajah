package ingatan.gajah.com.ingatangajah.enum

object AppConstant {

    const val KEY_GAME_FACE = "KEY_GAME_FACE"

    const val KEY_GAME_FACE_RANDOM = "KEY_GAME_FACE_RANDOM"

    const val KEY_MEMORIZE = "KEY_MEMORIZE"

    const val KEY_RECALL = "KEY_RECALL"

    const val KEY_RESULT = "KEY_RESULT"

    const val KEY_GAME_ID = "KEY_GAME_ID"

    const val LOGIN_TOKEN_HEADER = "X-Mega-Login-Token"

    const val ANONYMOUS_TOKEN_HEADER = "X-Mega-App-Token"

    const val INPUT_TYPE_PASSWORD = 129

    const val INPUT_TYPE_EMAIL = 33

    const val INPUT_TYPE_PHONE = 3

    const val KEY_GAME_STATE = "KEY_GAME_STATE"

    const val STATE_MEMORIZE = 0

    const val STATE_RECALL = 1

    const val STATE_ANSWER = 1

    const val STATE_QUESTION = 2

    const val KEY_GAME_REVIEW = "KEY_GAME_REVIEW"

    const val KEY_REVIEW_DATA = "KEY_REVIEW_DATA"

    const val GAME_NUMBER = 1

    const val GAME_WORDS = 2

    const val GAME_FACE = 3

    const val GAME_IMAGE = 4

    const val KEY_GAME_NUMBER = "KEY_GAME_NUMBER"

    const val KEY_GAME_WORDS = "KEY_GAME_WORDS"

    const val KEY_GAME_IMAGE = "KEY_GAME_IMAGE"

    const val KEY_GAME_IMAGE_RANDOM = "KEY_GAME_IMAGE_RANDOM"

    const val KEY_GAME_WORD_RANDOM = "KEY_GAME_IMAGE_RANDOM"

    const val KEY_POSITION = "KEY_POSITION"
    const val KEY_GAME_QUESTION = "KEY_GAME_QUESTION"
}