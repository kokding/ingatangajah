package ingatan.gajah.com.ingatangajah.view.game.number

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.PostGameNumberAnswerInteractor
import ingatan.gajah.com.ingatangajah.repository.request.GameNumberAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameNumberResultResponse
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import retrofit2.Response

class GameNumberPresenter(val view: GameNumberView, val disposableManager: DisposableManager, context: Context): AnkoLogger{

    val preferenceManager: PreferenceManager = PreferenceManager(context)
    val postGameNumberAnswerInteractor: PostGameNumberAnswerInteractor = PostGameNumberAnswerInteractor()

    fun postAnswer(gameId: Int, answer: GameNumberAnswerRequest) {
        info("Result "+answer.toString())
        val dispose = postGameNumberAnswerInteractor.postGameNumberAnswer(gameId,
                answer, preferenceManager.getAnonymousToken())
                .subscribeWith(object : DisposableObserver<Response<GameNumberResultResponse>>(){
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameNumberResultResponse>) {
                info("Result"+t.body().toString())
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.goToResult(t.body())
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }

}