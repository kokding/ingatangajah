package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.utils.setImage
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_face_answer.*
import org.jetbrains.anko.sdk25.coroutines.textChangedListener


class GameFaceAnswerAdapter(private val context: Context?, private val data: List<GameOrder>)
    : RecyclerView.Adapter<GameFaceAnswerAdapter.GameOrderViewHolder>() {

    lateinit var listener: (GameOrder) -> Unit

    var state: Int = AppConstant.STATE_MEMORIZE


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameOrderViewHolder = GameOrderViewHolder(LayoutInflater.from(context).inflate(R.layout.item_face_answer, parent, false))

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int = position

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: GameOrderViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        holder.bindItem(data[position], listener, state)
    }

    class GameOrderViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: GameOrder, listener: (GameOrder) -> Unit, state: Int) {
            //init UI
//            tvAnswer.visibility = if (state == AppConstant.STATE_MEMORIZE) View.VISIBLE else View.GONE
//            edtGame.visibility = if (state == AppConstant.STATE_RECALL) View.VISIBLE else View.GONE
            setImage(containerView, item.img, pb_img, img_game)
            containerView.setOnClickListener { listener(item) }
        }
    }
}