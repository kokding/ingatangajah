package ingatan.gajah.com.ingatangajah.repository.model

import com.google.gson.annotations.SerializedName

data class Schedule(

        @SerializedName("idEvent")
        val scheduleId: String,
        @SerializedName("idSoccerXML")
        val soccerXmlId: String,
        @SerializedName("strEvent")
        val scheduleName: String,
        @SerializedName("strFilename")
        val scheduleFullName: String,
        @SerializedName("strSport")
        val sportName: String,
        @SerializedName("idLeague")
        val leagueId: String,
        @SerializedName("strLeague")
        val leagueName: String,
        @SerializedName("strSeason")
        val season: String,
        @SerializedName("strHomeTeam")
        val homeTeam: String,
        @SerializedName("strAwayTeam")
        val awayTeam: String,
        @SerializedName("intRound")
        val matchRound: String,
        @SerializedName("dateEvent")
        val scheduleDate: String,
        @SerializedName("strDate")
        val slashDate: String,
        @SerializedName("strTime")
        val startTime: String,
        @SerializedName("idHomeTeam")
        val homeTeamId: String,
        @SerializedName("idAwayTeam")
        val awayTeamId: String,
        @SerializedName("strLocked")
        val strlocked: String
)