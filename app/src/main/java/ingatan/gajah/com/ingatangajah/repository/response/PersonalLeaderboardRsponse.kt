package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PersonalLeaderboardRsponse(

        @Expose
        @SerializedName("code")
        val code: Int,
        @Expose
        @SerializedName("status")
        val status: String,
        @Expose
        @SerializedName("data")
        val personalLeaderboard: PersonalLeaderboard)
