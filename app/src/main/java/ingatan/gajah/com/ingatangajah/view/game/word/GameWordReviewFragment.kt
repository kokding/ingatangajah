package ingatan.gajah.com.ingatangajah.view.game.word

import android.support.v7.widget.LinearLayoutManager
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.AnswerResultWord
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResultResponse
import ingatan.gajah.com.ingatangajah.view.adapter.GameWordReviewAdapter
import kotlinx.android.synthetic.main.fragment_game_word_review.*
import org.jetbrains.anko.bundleOf


class GameWordReviewFragment : BaseFragment(), GameWordReviewActivity.WordShowAnswerListener {

    private lateinit var adapter: GameWordReviewAdapter
    var gameQuestion: MutableList<AnswerResultWord> = mutableListOf()
    private var state: Int = AppConstant.STATE_MEMORIZE
    private var showAnswer = true

    companion object {
        fun newInstance(category: GameWordResultResponse, state: Int, coulmn: Int, gameQuestion: List<AnswerResultWord>) = GameWordReviewFragment().apply {
            arguments = bundleOf(
                    AppConstant.KEY_GAME_WORDS to category,
                    AppConstant.KEY_POSITION to coulmn,
                    AppConstant.KEY_GAME_STATE to state,
                    AppConstant.KEY_GAME_QUESTION to gameQuestion)
        }
    }


    override fun getLayoutResource(): Int = R.layout.fragment_game_word_review

    override fun initLib() {
    }

    override fun initIntent() {
        gameQuestion = arguments?.getParcelableArrayList(AppConstant.KEY_GAME_QUESTION)!!
    }

    override fun initUI() {
        GameWordReviewActivity.setWordShowAnswerListener(this)
        adapter = GameWordReviewAdapter(context, gameQuestion)
        adapter.setHasStableIds(true)
        adapter.state = AppConstant.STATE_ANSWER
        rvWord.layoutManager = LinearLayoutManager(context)
        rvWord.adapter = adapter

        adapter.notifyDataSetChanged()
    }

    override fun initAction() {
        adapter.listener = {}
        btnShowAnswer.setOnClickListener {
            showAnswer = !showAnswer
            adapter.state = if (showAnswer) AppConstant.STATE_ANSWER else AppConstant.STATE_QUESTION
            btnShowAnswer.text = if (!showAnswer) "Show Answer" else "Show Question"
            adapter.notifyDataSetChanged()
        }
    }

    override fun initProcess() {
    }

    override fun onShowButtonClicked() {
        showAnswer = !showAnswer
        adapter.state = if (showAnswer) AppConstant.STATE_ANSWER else AppConstant.STATE_QUESTION
        btnShowAnswer.text = if (!showAnswer) "Show Answer" else "Show Question"
        adapter.notifyDataSetChanged()
    }

    override fun setUserVisibleHint(isFragmentVisible_: Boolean) {
        super.setUserVisibleHint(true)
        if (this.isVisible) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_) {
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        showAnswer = true
        btnShowAnswer.text = if (!showAnswer) "Show Answer" else "Show Question"
    }
}
