package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.GameFaceAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostFaceAnswerInteractor(){
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postFaceAnswer(gameId:Int, answer: GameFaceAnswerRequest, token: String): Observable<Response<GameFaceResultResponse>> {

        return apiManager
                .postFaceAnswer(
                        gameId = gameId,
                        loginToken = "Bearer "+token,
                        answerRequest = answer
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}