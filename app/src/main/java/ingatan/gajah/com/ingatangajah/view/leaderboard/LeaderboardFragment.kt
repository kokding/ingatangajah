package ingatan.gajah.com.ingatangajah.view.leaderboard


import android.support.v7.widget.LinearLayoutManager
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.repository.response.Data
import ingatan.gajah.com.ingatangajah.repository.response.PersonalLeaderboard
import ingatan.gajah.com.ingatangajah.utils.gone
import ingatan.gajah.com.ingatangajah.utils.setImage
import ingatan.gajah.com.ingatangajah.utils.visible
import ingatan.gajah.com.ingatangajah.view.adapter.HomeMenuAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.LeaderboardAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.model.HomeMenu
import ingatan.gajah.com.ingatangajah.view.adapter.model.Leaderboard
import kotlinx.android.synthetic.main.fragment_leaderboard.*
import kotlinx.android.synthetic.main.item_my_score.*
import org.jetbrains.anko.support.v4.toast

class LeaderboardFragment : BaseFragment(), LeaderboardView {
    private lateinit var presenter: LeaderboardPresenter
    var disposableManager = DisposableManager()
    private var gameId: Int = 0

    private var leaderboards: MutableList<Leaderboard> = mutableListOf()
    private lateinit var leaderboardAdapter: LeaderboardAdapter

    private var menus: MutableList<HomeMenu> = mutableListOf()
    private lateinit var menuAdapter: HomeMenuAdapter

    override fun getLayoutResource(): Int = R.layout.fragment_leaderboard

    override fun initLib() {
        presenter = LeaderboardPresenter(this, disposableManager, context)
    }

    override fun initIntent() {
    }

    override fun initUI() {
        leaderboardAdapter = LeaderboardAdapter(context, leaderboards)
        rvLeaderboard.layoutManager = LinearLayoutManager(context)
        rvLeaderboard.adapter = leaderboardAdapter

        menuAdapter = HomeMenuAdapter(context, menus)
        menuAdapter.isHorizontal = true
        rv_menu.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rv_menu.adapter = menuAdapter
        initData()
    }

    private fun initData() {
        val names = resources.getStringArray(R.array.menu_name)
        val imgs = resources.obtainTypedArray(R.array.menu_icon)
        val ids = resources.getIntArray(R.array.menu_id)
        //create club item array
        menus.add(HomeMenu("Overall", R.drawable.icons_ingatan_gajah, 0, true))
        names.forEachIndexed { index, s ->
            menus.add(HomeMenu(s, imgs.getResourceId(index, 0), ids.get(index)))
        }

        imgs.recycle()
    }

    override fun initAction() {
        leaderboardAdapter.listener = {}
        menuAdapter.listener = { it, position ->
            menus.forEach { it.isSelected = false }
            it.isSelected = true
            menuAdapter.notifyDataSetChanged()
            gameId = it.id
            callLeaderboardData()
        }
    }

    override fun initProcess() {
    }

    override fun onResume() {
        super.onResume()
        initLeaderboardToMain()
        callLeaderboardData()
    }

    private fun initLeaderboardToMain() {
        menus.forEach { it.isSelected = false }
        menus.get(0).isSelected = true
        menuAdapter.notifyDataSetChanged()
    }

    private fun callLeaderboardData() {
        if (this.leaderboards.size > 0) this.leaderboards.clear()
        presenter.getLeaderboard(gameId)
        presenter.getPersonalLeaderboard(gameId)
    }

    override fun onPause() {
        super.onPause()
        disposableManager.dispose()
    }

    override fun error(message: String?) {
        toast(message.toString())
    }

    override fun hideLoading() {
        pbLeaderboard.gone()
        rvLeaderboard.visible()
    }

    override fun showData(leaderboards: List<Leaderboard>?) {
        if (this.leaderboards.size > 0) this.leaderboards.clear()
        this.leaderboards.addAll(leaderboards!!.toMutableList())
        leaderboardAdapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        pbLeaderboard.visible()
        rvLeaderboard.gone()
    }

    override fun showDataPersonal(personalLeaderboard: PersonalLeaderboard?, userCredential: Data?) {
        tvRank.text = personalLeaderboard?.rank
        tvName.text = userCredential?.profile?.username
        setImage(context!!, userCredential?.profile?.avatar
                ?: "", R.drawable.icons_ingatan_gajah, imgProfile)
        tvPoint.text = personalLeaderboard?.points + " pts"
    }
}
