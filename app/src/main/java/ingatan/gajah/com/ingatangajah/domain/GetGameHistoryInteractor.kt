package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.R.array.recall
import ingatan.gajah.com.ingatangajah.R.array.rows
import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResponse
import ingatan.gajah.com.ingatangajah.repository.response.HistoryResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class GetGameHistoryInteractor{
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun getGameHistory(game: Int, token: String, limit: Int): Observable<Response<HistoryResponse>> {

        return apiManager
                .getGameHistory(
                        gameId = game,
                        loginToken = token,
                        limit = limit
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}