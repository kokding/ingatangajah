package ingatan.gajah.com.ingatangajah.view.splash

import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.GetAnonymousTokenInteractor
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.response.ResponseModel
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver

class SplashPresenter(val view: SplashView, val disposableManager: DisposableManager?, splashScreenActivity: SplashScreenActivity) {
    private var getAnonymousTokenInteractor: GetAnonymousTokenInteractor = GetAnonymousTokenInteractor()
    private val preferenceManager: PreferenceManager = PreferenceManager(splashScreenActivity)
    fun getAnonymousToken() {
        if (preferenceManager.notLoggedIn()) {
            val dispose = getAnonymousTokenInteractor.getAnonymousToken().subscribeWith(object : DisposableObserver<retrofit2.Response<ResponseModel>>() {
                override fun onComplete() {
                }

                override fun onNext(t: retrofit2.Response<ResponseModel>) {
                    if (preferenceManager.notLoggedIn()) {
                        preferenceManager.setAnonymousToken(t.headers().get(AppConstant.ANONYMOUS_TOKEN_HEADER)
                                ?: "")
                        view.goToLogin(t.headers().get(AppConstant.ANONYMOUS_TOKEN_HEADER))
                    }
                }

                override fun onError(e: Throwable) {
                }
            })
            disposableManager?.add(dispose)
        } else {
            view.goToMainActivity()
        }
    }

}