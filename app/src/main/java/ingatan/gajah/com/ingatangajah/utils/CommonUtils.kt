package ingatan.gajah.com.ingatangajah.utils

import android.annotation.SuppressLint
import android.content.Context
import android.text.format.DateUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

fun isEmailValid(email: String): Boolean {
    val pattern: Pattern
    val matcher: Matcher
    val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    pattern = Pattern.compile(EMAIL_PATTERN)
    matcher = pattern.matcher(email)
    return matcher.matches()
}

fun getCurrency(price: Double): String {
    val indonesia = Locale("id", "ID")
    val swedishFormat = NumberFormat.getCurrencyInstance(indonesia)
    return getNormalizedValue(swedishFormat.format(price))
}

fun getNormalizedValue(value: String): String {
    return value.substring(2, value.length)
}

fun getReadableHumanDate(date: String): String {
    val months = arrayOf("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember")
    //2018-06-23 09:16:00
    val d = date.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val m = d[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

    return m[2] + " " + months[Integer.parseInt(m[1]) - 1] + " " + m[0]
}

fun getReadableHumanDateWithTime(date: String): String {
    val months = arrayOf("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des")
    //2017-08-13
    val d = date.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val m = d[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val h = d[1].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val readableDate = m[2] + " " + months[Integer.parseInt(m[1]) - 1] + " " + m[0]
    val readableTime = h[0] + ":" + h[1]

    return "$readableDate    $readableTime"
}

fun getRelativeTime(unformatted: String): String {
    var formatted = ""

    val date = unformatted.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val hours = date[1].split(".000Z".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

    formatted = date[0] + " " + hours[0]

    val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var d: Date?
    try {
        d = df.parse(formatted)
    } catch (e: ParseException) {
        e.printStackTrace()
        d = null
    }

    var timePassed: CharSequence = ""
    if (d != null) {
        val epoch = d.time
        timePassed = DateUtils.getRelativeTimeSpanString(epoch, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS)
    }

    return timePassed.toString()

}

fun getRelativeTime(timeInMillis: Long): String {
    return DateUtils.getRelativeTimeSpanString(timeInMillis * 1000,
            System.currentTimeMillis(), 0L, DateUtils.FORMAT_ABBREV_ALL).toString()
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

@SuppressLint("SimpleDateFormat")
fun toReadableDate(date: String?): String? {
    val dateFormat = SimpleDateFormat("dd/MM/yyyy")
    val formatedDate = dateFormat.parse(date)
    return SimpleDateFormat("EEE, dd MMM yyy").format(formatedDate)
}

fun toMinuteSecondTimer(minute: Long, second: Int): String {
    return "${(minute / 60)}.${second}"
}

fun showKeyboard(view: View, context: Context?) {
    if (view.requestFocus()) {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
}

fun hideSoftKeyboard(view: View, context: Context?) {
    val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm!!.hideSoftInputFromWindow(view.windowToken, 0)
}

fun getGameName(gameId: Int): String {
    return when (gameId) {
        AppConstant.GAME_NUMBER -> "Speed Numbers"
        AppConstant.GAME_WORDS -> "Random Words"
        AppConstant.GAME_FACE -> "Names & Faces"
        AppConstant.GAME_IMAGE -> "Random Images"
        else -> "Main"
    }
}

fun getGameImage(gameId: Int): Int {
    return when (gameId) {
        AppConstant.GAME_NUMBER -> R.drawable.game_logo_angka
        AppConstant.GAME_WORDS -> R.drawable.game_logo_kata
        AppConstant.GAME_FACE -> R.drawable.game_logo_wajah
        AppConstant.GAME_IMAGE -> R.drawable.game_logo_gambar
        else -> R.drawable.icons_ingatan_gajah
    }
}