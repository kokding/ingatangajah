package ingatan.gajah.com.ingatangajah.repository.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameResult(

        @SerializedName("total_question")
        val totalQuestion: Int,
      
        @SerializedName("total_answers")
        val totalAnswers: Int,
      
        @SerializedName("correct")
        val correct: Int,
      
        @SerializedName("incorrect")
        val incorrect: Int,
      
        @SerializedName("blank")
        val blank: Int,
      
        @SerializedName("points")
        val points: Int) : Parcelable