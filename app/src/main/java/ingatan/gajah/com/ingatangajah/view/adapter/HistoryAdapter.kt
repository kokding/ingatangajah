package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.repository.model.History
import ingatan.gajah.com.ingatangajah.utils.getGameImage
import ingatan.gajah.com.ingatangajah.utils.getGameName
import ingatan.gajah.com.ingatangajah.utils.setImage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_history.*


class HistoryAdapter(private val context: Context, private val data: List<History>)
    : RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

    lateinit var listener: (History) -> Unit

    var gameId = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder = HistoryViewHolder(LayoutInflater.from(context).inflate(R.layout.item_history, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) = holder.bindItem(data[position], listener, gameId)

    class HistoryViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: History, listener: (History) -> Unit, gameId: Int) {
            //init UI
            setImage(containerView, getGameImage(gameId), imgIcon)
            tvGameName.text = getGameName(gameId)
            tvCreatedAt.text = item.createdAt
            tvPoints.text = item.points.toString() + " pts"
            tvGameStatus.text = "Correct ${item.correct} Incorrect ${item.incorrect} from ${item.totalQuestion} questions."
        }
    }
}