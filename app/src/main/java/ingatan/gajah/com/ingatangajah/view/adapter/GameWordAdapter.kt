package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_word.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.info
import org.jetbrains.anko.sdk25.coroutines.textChangedListener


class GameWordAdapter(private val context: Context?, private val data: List<GameOrder>, private val columns: Int, private val rows: Int)
    : RecyclerView.Adapter<GameWordAdapter.GameWordViewHolder>() {

    lateinit var listener: (GameOrder) -> Unit

    var state: Int = AppConstant.STATE_MEMORIZE

    var isHide = false

    override fun getItemViewType(position: Int): Int = position

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameWordViewHolder = GameWordViewHolder(LayoutInflater.from(context).inflate(R.layout.item_word, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: GameWordViewHolder, position: Int) = holder.bindItem(data[position], listener, state, isHide, columns, rows)

    class GameWordViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer, AnkoLogger {

        companion object {
            var listener: OnWordChangedListener? = null

            fun setWordChangedListner(listener: OnWordChangedListener) {
                this.listener = listener
            }
        }

        fun bindItem(item: GameOrder, listener: (GameOrder) -> Unit, state: Int, hide: Boolean, columns: Int, rows: Int) {
            info{"word adapter state $state"}
            tvAnswer.visibility = if (state == AppConstant.STATE_MEMORIZE) View.VISIBLE else View.GONE
            edtGame.visibility = if (state == AppConstant.STATE_RECALL) View.VISIBLE else View.GONE
            when (hide) {
                true -> {
                    tvAnswer.text = ""
                    tvAnswer.backgroundColor = containerView.resources.getColor(R.color.grey_dot)
                }
                false -> {
                    tvAnswer.text = item.question
                    tvAnswer.backgroundColor = containerView.resources.getColor(R.color.real_white)
                }
            }
            edtGame.textChangedListener {
                afterTextChanged {
                    item.answer = it?.toString()
                    edtGame.background = containerView.resources.getDrawable(
                            if (it.isNullOrEmpty()) R.drawable.edittext_empty_selector
                            else R.drawable.edittext_answered_selector)
                    GameWordViewHolder.listener?.changeAnswer(adapterPosition+(rows*columns), it?.toString())
                }
            }
            containerView.setOnClickListener { listener(item) }
            tvLabel.text = "${adapterPosition+(rows*columns)+1}"
            edtGame.background = containerView.resources.getDrawable(
                    if (item.answer.isNullOrEmpty()) R.drawable.edittext_empty_selector
                    else R.drawable.edittext_answered_selector)
            if (!item.answer.isNullOrEmpty()) edtGame.setText(item.answer)
        }

        interface OnWordChangedListener {
            fun changeAnswer(position: Int, answer: String?)
        }
    }
}