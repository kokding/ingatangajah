package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.SerializedName

data class ProfileResponse(

        @SerializedName("code")
        val code: Int,
        @SerializedName("status")
        val status: String,
        @SerializedName("data")
        val profile: Profile)
