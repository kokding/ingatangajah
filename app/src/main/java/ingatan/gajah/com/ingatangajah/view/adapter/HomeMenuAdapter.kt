package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.utils.setImage
import ingatan.gajah.com.ingatangajah.view.adapter.model.HomeMenu
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_menu.*
import kotlinx.android.synthetic.main.item_menu_horizontal.*


class HomeMenuAdapter(private val context: Context?, private val data: List<HomeMenu>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: (HomeMenu, Int) -> Unit

    var isHorizontal: Boolean = false

    var mContext: Context? = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (isHorizontal)
            return MenuHorizontalViewHolder(LayoutInflater.from(context).inflate(R.layout.item_menu_horizontal, parent, false))
        else
            return MenuViewHolder(LayoutInflater.from(context).inflate(R.layout.item_menu, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MenuHorizontalViewHolder)
            holder.bindItem(data[position], listener)
        else if (holder is MenuViewHolder)
            holder.bindItem(data[position], listener)
    }

    class MenuViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: HomeMenu, listener: (HomeMenu, Int) -> Unit) {
            //init UI
            tv_title.text = item.name
            Glide.with(containerView).load(item.image).into(img_icon)
            containerView.setOnClickListener { listener(item, adapterPosition) }
        }
    }

    class MenuHorizontalViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: HomeMenu, listener: (HomeMenu, Int) -> Unit) {
            //init UI
            tv_name.text = item.name
            setImage(containerView, item.image, img_icon_menu)
            cv_menu.isSelected = item.isSelected
            containerView.setOnClickListener {
                listener(item, adapterPosition)
            }
        }
    }
}