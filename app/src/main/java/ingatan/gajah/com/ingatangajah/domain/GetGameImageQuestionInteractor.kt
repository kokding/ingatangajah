package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.response.GameImageResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response


class GetGameImageQuestionInteractor {

    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun getGameImageQuestion(game: Int, token: String, columns: String, rows: String, memorizeTime: String, recall: String): Observable<Response<GameImageResponse>> {

        return apiManager
                .getGameImageQuestion(
                        gameId = game,
                        loginToken = token,
                        colum = columns,
                        rows = rows,
                        memorizeTime = memorizeTime,
                        recall = recall
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}