package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.request.PostAvatarRequest
import ingatan.gajah.com.ingatangajah.repository.response.PostAvatarResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PostAvatarInteractor() {
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun postAvatar(token: String, request: PostAvatarRequest): Observable<Response<PostAvatarResponse>> {

        return apiManager
                .postAvatar(loginToken = token,
                        avatar = request.avatar!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
