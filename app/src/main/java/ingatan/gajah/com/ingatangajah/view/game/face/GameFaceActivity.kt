package ingatan.gajah.com.ingatangajah.view.game.face

import android.support.v4.app.Fragment
import android.view.MenuItem
import android.view.WindowManager
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResponse
import ingatan.gajah.com.ingatangajah.utils.MessageFactory
import ingatan.gajah.com.ingatangajah.utils.gone
import ingatan.gajah.com.ingatangajah.utils.toMinuteSecondTimer
import ingatan.gajah.com.ingatangajah.utils.visible
import ingatan.gajah.com.ingatangajah.view.game.creategame.CreateGameActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_game_face.*
import org.jetbrains.anko.startActivity
import java.util.concurrent.TimeUnit


class GameFaceActivity : BaseActivity() {

    private lateinit var memorize: String
    private lateinit var recall: String
    private lateinit var data: GameFaceResponse
    private lateinit var dataRandom: GameFaceResponse
    private lateinit var disposable: Disposable
    private lateinit var memorizeFragment: Fragment
    private lateinit var recallFragment: Fragment
    private var isLinearMode = true

    private var isReviewMode: Boolean = false
    private var isHideQuestion: Boolean = false
    private var state: Int = AppConstant.STATE_MEMORIZE
    private var userMemorizeTime: Long = 0
    private var userRecallTime: Long = 0

    companion object {
        var listener: GameEndListener? = null

        fun setGameListener(listener: GameEndListener) {
            this.listener = listener
        }
    }

    override fun getLayoutResource(): Int = R.layout.activity_game_face

    override fun initIntent() {
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
        data = intent.getParcelableExtra(AppConstant.KEY_GAME_FACE)
        dataRandom = intent.getParcelableExtra(AppConstant.KEY_GAME_FACE_RANDOM)
        memorize = intent.getStringExtra(AppConstant.KEY_MEMORIZE)
        recall = intent.getStringExtra(AppConstant.KEY_RECALL)

        isReviewMode = intent.getBooleanExtra(AppConstant.KEY_GAME_REVIEW, false)
        data.gameSettings.memorizeTime = memorize.toInt() * 60
        data.gameSettings.recallTime = recall.toInt() * 60
        memorizeFragment = GameFaceFragment.newInstance(data, AppConstant.STATE_MEMORIZE)
        dataRandom.gameSettings = data.gameSettings
        recallFragment = GameFaceFragment.newInstance(dataRandom, AppConstant.STATE_RECALL)
//        recallFragment = GameFaceAnswerFragment.newInstance(dataRandom, AppConstant.STATE_RECALL)
        initMemorizeTime()
    }

    override fun initAction() {
        btnVisible.setOnClickListener {
            isHideQuestion = !isHideQuestion
            btnVisible.setImageResource(
                    if (isHideQuestion) R.drawable.ic_visibility_black_24dp
                    else R.drawable.ic_visibility_off_black_24dp)
            listener?.onVisibleMemorizeClicked(isHideQuestion)
        }

        btnReload.setOnClickListener {
            setFragmentTransaction(R.id.container_game, memorizeFragment, false)
        }
    }

    override fun initUI() {
        tvTitle.text = getString(R.string.title_face)
        tvTimer.text = toMinuteSecondTimer(memorize.toLong() * 60, 0)
        container_game.gone()
        setFragmentTransaction(R.id.container_game, memorizeFragment, false)
        handleGameStateChange()
    }

    private fun handleGameStateChange() {
        btnEnd.text = if (state == AppConstant.STATE_MEMORIZE) getString(R.string.btn_recall) else getString(R.string.btn_end_game)
        //TODO if want to visible reload button
//        if (state == AppConstant.STATE_MEMORIZE) btnReload.visible() else btnReload.gone()
        btnReload.gone()
        btnEnd.setOnClickListener {
            if (state == AppConstant.STATE_MEMORIZE) {
                MessageFactory.showConfirmationAlert(this@GameFaceActivity,
                        getString(R.string.title_recall_now),
                        getString(R.string.message_recal_now),
                        getString(R.string.btn_recall_now),
                        object : MessageFactory.Companion.MyDialogActionListener {
                            override fun onPositiveAnswer() {
                                disposable.dispose()
                                memorizeEnd()
                            }

                            override fun onNegativeAnswer() {
                            }
                        },
                        getString(R.string.btn_cancel),
                        object : MessageFactory.Companion.MyDialogActionListener {
                            override fun onPositiveAnswer() {

                            }

                            override fun onNegativeAnswer() {
                            }
                        })
            } else {
                MessageFactory.showConfirmationAlert(this@GameFaceActivity,
                        getString(R.string.title_end_game),
                        getString(R.string.message_end_now),
                        getString(R.string.btn_end_now),
                        object : MessageFactory.Companion.MyDialogActionListener {
                            override fun onPositiveAnswer() {
                                disposable.dispose()
                                recallEnd()
                            }

                            override fun onNegativeAnswer() {
                            }
                        },
                        getString(R.string.btn_cancel),
                        object : MessageFactory.Companion.MyDialogActionListener {
                            override fun onPositiveAnswer() {

                            }

                            override fun onNegativeAnswer() {
                            }
                        })
            }
        }
    }

    private fun initMemorizeTime() {
        gameToolbar.visible()
        MessageFactory.showDoneAlert(this@GameFaceActivity,
                getString(R.string.title_ready_to_memorize),
                getString(R.string.message_ready_to_memorize),
                getString(R.string.btn_start_memorize),
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {
                        startMemorizeCountdown()
                    }

                    override fun onNegativeAnswer() {
                    }
                })
    }

    private fun startMemorizeCountdown() {
        countDown.visible()
        val counter = 60
        val time: Long = (counter + 1).toLong()
        disposable = Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .repeat()
                .takeUntil { it == time }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        59.toLong() -> countDown.text = "Neurons on the ready?!"
                        60.toLong() -> countDown.text = "GO !"
                        time -> {
                            countDown.gone()
                            countDown.text = ""
                            btnVisible.visible()
                            container_game.visible()
                            startMemorizeCounter()
                        }
                        else -> countDown.text = (counter - it).toString()
                    }
                }
    }

    private fun startRecallCountdown() {
        countDown.visible()
        btnVisible.gone()
        state = AppConstant.STATE_RECALL
        handleGameStateChange()
        setFragmentTransaction(R.id.container_game, recallFragment, false)
        val time: Long = 6
        disposable = Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .repeat()
                .takeUntil { it == time }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        0.toLong() -> countDown.text = "5"
                        1.toLong() -> countDown.text = "4"
                        2.toLong() -> countDown.text = "3"
                        3.toLong() -> countDown.text = "2"
//                        4.toLong() -> countDown.text = "Neurons on the ready"
                        4.toLong() -> countDown.text = "1"
                        5.toLong() -> countDown.text = "GO !"
                        else -> {
                            countDown.gone()
                            container_game.visible()
                            startRecallCounter()
                        }
                    }
                }
    }

    private fun startMemorizeCounter() {
        var second = 60
        var time: Long = (memorize.toLong() * 60)
//        val time: Long = 5
        disposable = Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .repeat()
                .takeUntil { it == time }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    tvTimer.text = toMinuteSecondTimer(
                            minute = time - it,
                            second = if (second == 60) 0 else second)
                    second--
                    if (second <= 0 && (it / 60).toInt() != 0) second = 60
                    userMemorizeTime = it
                    if (it == time) memorizeEnd()
                }
    }

    private fun recallEnd() {
        listener?.onGameEnd(userMemorizeTime, userRecallTime)
    }

    private fun startRecallCounter() {
        var second = 60
        var time: Long = (recall.toLong() * 60)
//        val time: Long = 5
        disposable = Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .repeat()
                .takeUntil { it == time }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    tvTimer.text = toMinuteSecondTimer(
                            minute = time - it,
                            second = if (second == 60) 0 else second)
                    second--
                    if (second <= 0 && (it / 60).toInt() != 0) second = 60
                    userRecallTime = it
                    if (it == time) recallEnd()
                }
    }

    private fun memorizeEnd() {
        container_game.gone()
        MessageFactory.showDoneAlert(this@GameFaceActivity,
                getString(R.string.title_time_to_recall),
                getString(R.string.message_time_to_recall),
                getString(R.string.btn_start_recall),
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {
                        startRecallCountdown()
                    }

                    override fun onNegativeAnswer() {
                    }
                })
    }

    override fun onBackPressed() {
        MessageFactory.showConfirmationAlert(this@GameFaceActivity,
                getString(R.string.title_close_game),
                getString(R.string.message_close_game),
                getString(R.string.btn_close_game),
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {
                        startActivity<CreateGameActivity>(AppConstant.KEY_GAME_ID to AppConstant.GAME_FACE)
                        finish()
                    }

                    override fun onNegativeAnswer() {
                    }
                },
                getString(R.string.btn_cancel),
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {

                    }

                    override fun onNegativeAnswer() {
                    }
                })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    interface GameEndListener {

        fun onGameEnd(userMemorizeTime: Long, userRecallTime: Long)

        fun onVisibleMemorizeClicked(isHide: Boolean)
    }

    override fun onPause() {
        super.onPause()
        disposable.dispose()
        MessageFactory.showDoneAlert(this@GameFaceActivity,
                getString(R.string.title_close_game), "You can not resume the game, you must start again",
                getString(R.string.btn_close_game),
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {
                        startActivity<CreateGameActivity>(AppConstant.KEY_GAME_ID to AppConstant.GAME_WORDS)
                        disposable.dispose()
                        finish()
                    }

                    override fun onNegativeAnswer() {
                    }
                })
    }

    override fun onResume() {
        super.onResume()
    }
}
