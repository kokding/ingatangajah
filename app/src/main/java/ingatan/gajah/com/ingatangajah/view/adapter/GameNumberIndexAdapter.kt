package ingatan.gajah.com.ingatangajah.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ingatan.gajah.com.ingatangajah.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_index.*


class GameNumberIndexAdapter(private val context: Context?, private val data: List<Int>)
    : RecyclerView.Adapter<GameNumberIndexAdapter.IntViewHolder>() {

    lateinit var listener: (Int) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntViewHolder = IntViewHolder(LayoutInflater.from(context).inflate(R.layout.item_index, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: IntViewHolder, position: Int) = holder.bindItem(data[position], listener)

    class IntViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        fun bindItem(item: Int, listener: (Int) -> Unit) {
            //init UI
            tvIndex.text = item.toString()
//            if (item % 2 == 0)
//                tvIndex.backgroundColor = containerView.resources.getColor(R.color.grey_dot)
//            else
//                tvIndex.backgroundColor = containerView.resources.getColor(R.color.real_white)
        }
    }
}