package ingatan.gajah.com.ingatangajah.repository.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import ingatan.gajah.com.ingatangajah.repository.model.Faces
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameImageResponse (
        @SerializedName("code")
        val code: Int,

        @SerializedName("status")
        val status: String,

        @SerializedName("game_settings")
        var gameSettings: GameSettings,

        @SerializedName("data")
        val data: List<Order>
) : Parcelable
