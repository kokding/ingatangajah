package ingatan.gajah.com.ingatangajah.view.game.number

import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.response.GameNumberResultResponse
import ingatan.gajah.com.ingatangajah.view.game.creategame.CreateGameActivity
import ingatan.gajah.com.ingatangajah.view.game.face.FaceReviewActivity
import ingatan.gajah.com.ingatangajah.view.main.MainActivity
import kotlinx.android.synthetic.main.activity_game_number_result.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.startActivity

class GameNumberResultActivity : BaseActivity() {

    private lateinit var data: GameNumberResultResponse

    override fun getLayoutResource(): Int = R.layout.activity_game_number_result

    override fun initIntent() {
        data = intent.getParcelableExtra(AppConstant.KEY_RESULT)
    }

    private fun initReviewData() {

    }

    override fun initUI() {
        tvPoints.text = "${data.gameResults.points} pts"
//        tvTime.text = data.gameResults.
        tvTrue.text = data.gameResults.correct.toString()
        tvFalse.text = data.gameResults.incorrect.toString()
        tvGameSetting.text = "${data.gameResults.blank} blank, from ${data.gameResults.totalQuestion} questions"
        initReviewData()
    }

    override fun initAction() {
        btnReview.setOnClickListener {
            startActivity<GameNumberReviewActivity>(
                    AppConstant.KEY_REVIEW_DATA to data)
        }
        btnRetry.setOnClickListener {
            startActivity<CreateGameActivity>(AppConstant.KEY_GAME_ID to AppConstant.GAME_NUMBER)
            finish()
        }
        btnPositive.setOnClickListener {
            startActivity<MainActivity>()
            finish()
        }
    }
}
