package ingatan.gajah.com.ingatangajah.view.home

import android.support.v7.widget.GridLayoutManager
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.Schedule
import ingatan.gajah.com.ingatangajah.repository.response.Data
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResponse
import ingatan.gajah.com.ingatangajah.repository.response.PersonalLeaderboard
import ingatan.gajah.com.ingatangajah.utils.setImage
import ingatan.gajah.com.ingatangajah.view.adapter.HomeMenuAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.model.HomeMenu
import ingatan.gajah.com.ingatangajah.view.game.creategame.CreateGameActivity
import ingatan.gajah.com.ingatangajah.view.game.order.GameOrderActivity
import ingatan.gajah.com.ingatangajah.view.game.word.GameWordFragment
import kotlinx.android.synthetic.main.fragment_homemenu.*
import kotlinx.android.synthetic.main.item_my_score.*
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

class HomeMenuFragment : BaseFragment(), HomeMenuView {

    lateinit var presenter: HomeMenuPresenter

    var disposable = DisposableManager()
    private var menus: MutableList<HomeMenu> = mutableListOf()
    private lateinit var menuAdapter: HomeMenuAdapter

    companion object {

        fun newInstance() = HomeMenuFragment().apply {
            arguments = bundleOf()
        }
    }

    override fun getLayoutResource(): Int = R.layout.fragment_homemenu

    override fun initLib() {
        presenter = HomeMenuPresenter(this, disposable, context)
    }

    override fun initIntent() {
    }

    override fun initUI() {
        menuAdapter = HomeMenuAdapter(context, menus)
        rv_menu.layoutManager = GridLayoutManager(context, 2)
        rv_menu.adapter = menuAdapter
        initData()
    }

    private fun initData() {
        val names = resources.getStringArray(R.array.menu_name)
        val imgs = resources.obtainTypedArray(R.array.menu_icon)
        //create club item array
        names.forEachIndexed { index, s ->
            menus.add(HomeMenu(s, imgs.getResourceId(index, 0), index))
        }

        imgs.recycle()
    }

    override fun initAction() {
        menuAdapter.listener = { it, position ->
            when (it.name) {
                "Speed Numbers" -> startActivity<CreateGameActivity>(AppConstant.KEY_GAME_ID to AppConstant.GAME_NUMBER)
                "Random Words" -> startActivity<CreateGameActivity>(AppConstant.KEY_GAME_ID to AppConstant.GAME_WORDS)
                "Names & Faces" -> startActivity<CreateGameActivity>(AppConstant.KEY_GAME_ID to AppConstant.GAME_FACE)
                "Random Images" -> startActivity<CreateGameActivity>(AppConstant.KEY_GAME_ID to AppConstant.GAME_IMAGE)
            }


        }
    }

    override fun initProcess() {
    }

    override fun showData(schedules: List<Schedule>) {
    }

    override fun onPause() {
        super.onPause()
        disposable.dispose()
    }

    override fun onResume() {
        super.onResume()
        presenter.getLeaderboard(0)
    }

    override fun userData(userCredential: Data) {
        tvName.text = userCredential.profile?.username
        tvRank.text = userCredential.profile?.rank.toString()
        setImage(context!!, userCredential.profile?.avatar
                ?: "", R.drawable.icons_ingatan_gajah, imgProfile)
    }

    override fun error(message: String?) {
        toast(message.toString())
    }

    override fun showDataPersonal(personalLeaderboard: PersonalLeaderboard?, userCredential: Data?) {
        tvRank.text = personalLeaderboard?.rank
        tvName.text = userCredential?.profile?.username
        setImage(context!!, userCredential?.profile?.avatar
                ?: "", R.drawable.icons_ingatan_gajah, imgProfile)
        tvPoint.text = personalLeaderboard?.points + " pts"
    }
}
