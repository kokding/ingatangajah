package ingatan.gajah.com.ingatangajah.repository.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameSettings(

        @SerializedName("columns")
        val columns: Int,

        @SerializedName("rows")
        val rows: Int,

        @SerializedName("slides")
        val slides: Int,

        @SerializedName("memorize_time")
        var memorizeTime: Int,

        @SerializedName("recall_time")
        var recallTime: Int,

        @SerializedName("user_memorize_time")
        var userMemorize: Int?,

        @SerializedName("user_recall_time")
        var userRecall: Int?

) : Parcelable