package ingatan.gajah.com.ingatangajah.view.game.face

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.PostFaceAnswerInteractor
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.request.GameFaceAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameFaceResultResponse
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import retrofit2.Response

class GameFacePresenter(val view: GameFaceView,val disposableManager: DisposableManager, context: Context): AnkoLogger {
    val preferenceManager: PreferenceManager = PreferenceManager(context)
    val postAnswerFaceAnswerInteractor: PostFaceAnswerInteractor = PostFaceAnswerInteractor()

    fun postAnswer(answer: GameFaceAnswerRequest) {
        val dispose = postAnswerFaceAnswerInteractor.postFaceAnswer(AppConstant.GAME_FACE,
                answer, preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<GameFaceResultResponse>>(){
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameFaceResultResponse>) {
                info("Result"+t.body().toString())
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.goToResult(t.body())
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposableManager.add(dispose)
    }


}