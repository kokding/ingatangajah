package ingatan.gajah.com.ingatangajah.repository.response

import com.google.gson.annotations.SerializedName

data class PostAvatarResponse(

        @SerializedName("code")
        val code: Int,

        @SerializedName("status")
        val status: String,

        @SerializedName("data")
        val postAvatar: PostAvatar)
