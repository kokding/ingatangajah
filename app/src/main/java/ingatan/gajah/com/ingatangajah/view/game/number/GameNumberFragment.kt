package ingatan.gajah.com.ingatangajah.view.game.number

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.request.GameNumberAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameNumberResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameNumberResultResponse
import ingatan.gajah.com.ingatangajah.utils.MessageFactory
import ingatan.gajah.com.ingatangajah.view.adapter.GameNumberAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.GameNumberIndexAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import kotlinx.android.synthetic.main.fragment_game_number.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.info
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast


class GameNumberFragment : BaseFragment(), GameNumberView, GameNumberActivity.GameEndListener, AnkoLogger {

    var gameOrder: MutableList<GameOrder> = mutableListOf()
    var answers: MutableList<Int> = mutableListOf()
    private var indexes: MutableList<Int> = mutableListOf()
    private val disposableManager = DisposableManager()
    lateinit var adapter: GameNumberAdapter
    lateinit var indexAdapter: GameNumberIndexAdapter
    private lateinit var presenter: GameNumberPresenter
    lateinit var data: GameNumberResponse
    private var state: Int = AppConstant.STATE_MEMORIZE

    companion object {

        fun newInstance(category: GameNumberResponse, state: Int) = GameNumberFragment().apply {
            arguments = bundleOf(
                    AppConstant.KEY_GAME_FACE to category,
                    AppConstant.KEY_GAME_STATE to state)
        }
    }

    override fun getLayoutResource(): Int = R.layout.fragment_game_number

    override fun initLib() {
        presenter = GameNumberPresenter(this, disposableManager, context!!)
        GameNumberActivity.setGameListener(this)
    }

    override fun initIntent() {
        data = arguments?.getParcelable(AppConstant.KEY_GAME_FACE)!!
        state = arguments?.getInt(AppConstant.KEY_GAME_STATE, AppConstant.STATE_MEMORIZE)!!

    }

    override fun initUI() {
        adapter = GameNumberAdapter(context, gameOrder)
        adapter.setHasStableIds(true)
        adapter.state = state
        when (state) {
            AppConstant.STATE_MEMORIZE, AppConstant.STATE_RECALL -> {
                rvGameNumber.layoutManager = GridLayoutManager(context, data.gameSettings.columns)
            }
        }
        rvGameNumber.adapter = adapter

        indexAdapter = GameNumberIndexAdapter(context, indexes)
        indexAdapter.setHasStableIds(true)
        rvIndex.layoutManager = LinearLayoutManager(context)
        rvIndex.adapter = indexAdapter
        indexes.addAll(1..data.gameSettings.rows)
        indexAdapter.notifyDataSetChanged()
        iniData()
    }

    override fun initAction() {
        adapter.listener = {

        }

        indexAdapter.listener = {}
    }

    override fun initProcess() {
    }

    override fun onGameEnd(userMemorizeTime: Long, userRecallTime: Long) {
        MessageFactory.showDoneAlert(context,
                getString(R.string.title_end_game),
                getString(R.string.message_end_game_to_result),
                getString(R.string.btn_end_game_to_result),
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {
//                        toast(gameOrder.toString())
                        gameOrder.forEach {
                            val answer: String = if (it.answer.isNullOrBlank()) "-1" else it.answer
                                    ?: "-1"
                            answers.add(answer.toInt())
                        }
                        data.gameSettings.userMemorize = userMemorizeTime.toInt()
                        data.gameSettings.userRecall = userRecallTime.toInt()
                        val answer = GameNumberAnswerRequest(data.gameSettings, data.data, answers)
                        info("MANTAP " + answer.toString())
                        presenter.postAnswer(AppConstant.GAME_NUMBER, answer)
                    }

                    override fun onNegativeAnswer() {
                    }
                })
    }

    override fun onVisibleMemorizeClicked(isHide: Boolean) {
        adapter.isHide = isHide
        adapter.notifyDataSetChanged()
    }

    private fun iniData() {
        data.data.forEach {
            gameOrder.add(GameOrder(
                    id = it,
                    img = "",
                    question = it.toString(),
                    answer = ""))
        }

        adapter.notifyDataSetChanged()
    }

    override fun onPause() {
        super.onPause()
        disposableManager.dispose()
    }

    override fun goToResult(body: GameNumberResultResponse?) {
        startActivity<GameNumberResultActivity>(AppConstant.KEY_RESULT to body)
        activity?.finish()
    }

    override fun error(message: String?) {
        toast(message ?: "Terjadi Kesalahan, Coba Beberapa saat lagi")
    }
}
