package ingatan.gajah.com.ingatangajah.view.game.number

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseActivity
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.GameNumberAnswerResult
import ingatan.gajah.com.ingatangajah.repository.response.GameNumberResultResponse
import ingatan.gajah.com.ingatangajah.view.adapter.GameNumberIndexAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.GameNumberReviewAdapter
import kotlinx.android.synthetic.main.activity_game_number_review.*

class GameNumberReviewActivity : BaseActivity() {
    override fun getLayoutResource(): Int = R.layout.activity_game_number_review

    private lateinit var data: GameNumberResultResponse
    private lateinit var adapter: GameNumberReviewAdapter
    private var answers: MutableList<GameNumberAnswerResult> = mutableListOf()
    private var showAnswer = true
    lateinit var indexAdapter: GameNumberIndexAdapter

    private var indexes: MutableList<Int> = mutableListOf()

    override fun initIntent() {
        data = intent.getParcelableExtra(AppConstant.KEY_REVIEW_DATA)
        adapter = GameNumberReviewAdapter(this@GameNumberReviewActivity, answers)
        adapter.setHasStableIds(true)
        adapter.state = AppConstant.STATE_ANSWER
        rvGameNumber.layoutManager = GridLayoutManager(this@GameNumberReviewActivity, data.gameSettings.columns)
        rvGameNumber.adapter = adapter
        answers.addAll(data.data)
        adapter.notifyDataSetChanged()

        indexAdapter = GameNumberIndexAdapter(this@GameNumberReviewActivity, indexes)
        indexAdapter.setHasStableIds(true)
        rvIndex.layoutManager = LinearLayoutManager(this@GameNumberReviewActivity)
        rvIndex.adapter = indexAdapter
        indexes.addAll(1..data.gameSettings.rows)
        indexAdapter.notifyDataSetChanged()

    }

    override fun initUI() {

    }

    override fun initAction() {
        btnShowAnswer.setOnClickListener {
            showAnswer = !showAnswer
            adapter.state = if (showAnswer) AppConstant.STATE_ANSWER else AppConstant.STATE_QUESTION
            btnShowAnswer.text = if (!showAnswer) "Show Answer" else "Show Question"
            adapter.notifyDataSetChanged()
        }
        adapter.listener = {

        }

        indexAdapter.listener = {}

        btnBack.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
