package ingatan.gajah.com.ingatangajah.base

import android.support.v7.widget.Toolbar

interface IBaseView {

    fun setupToolbar(toolbar: Toolbar, title: String, isChild: Boolean)

    fun setupToolbar(toolbar: Toolbar, isChild: Boolean)

    fun setupToolbar(title: String, isChild: Boolean)
}