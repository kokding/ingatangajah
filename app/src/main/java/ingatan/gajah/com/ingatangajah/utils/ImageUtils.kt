package ingatan.gajah.com.ingatangajah.utils

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target


fun <T : ImageView> setImage(c: Context, imageRes: Int, t: T) {
    Glide.with(c)
            .load(imageRes)
            .into(t)
}

fun <T : ImageView> setImage(c: Context, imageUri: String, t: T) {
    Glide.with(c)
            .load(imageUri)
            .into(t)
}

fun <T : ImageView> setImage(c: View, imageRes: Int, t: T) {
    Glide.with(c)
            .load(imageRes)
            .into(t)
}

fun <T : ImageView> setImage(c: View, imageUri: String, t: T) {
    Glide.with(c)
            .load(imageUri)
            .into(t)
}

fun <T : ImageView> setImage(c: Context, imageUri: String, placeholderResourceId: Int, t: T) {

    val options = RequestOptions()
            .centerCrop()
            .error(placeholderResourceId)

    Glide.with(c)
            .load(imageUri)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: View, imageUri: String, placeholderResourceId: Int, t: T) {

    val options = RequestOptions()
            .centerCrop()
            .error(placeholderResourceId)

    Glide.with(c)
            .load(imageUri)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: Context, imageRes: Int, placeholderResourceId: Int, t: T) {
    val options = RequestOptions()
            .centerCrop()
            .error(placeholderResourceId)

    Glide.with(c)
            .load(imageRes)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: Context, imageRes: Int, placeHolderResourceId: Int, errorResourceId: Int, t: T) {
    val options = RequestOptions()
            .centerCrop()
            .placeholder(placeHolderResourceId)
            .error(errorResourceId)

    Glide.with(c)
            .load(imageRes)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: Context, imageRes: Int, placeHolderDrawable: Int, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .centerCrop()
            .placeholder(placeHolderDrawable)
            .error(errorDrawable)

    Glide.with(c)
            .load(imageRes)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: Context, imageRes: String, placeHolderDrawable: Int, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .centerCrop()
            .placeholder(placeHolderDrawable)
            .error(errorDrawable)

    Glide.with(c)
            .load(imageRes)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: Context, imageRes: Int, progressBar: ProgressBar, t: T) {
    progressBar.visible()
    Glide.with(c)
            .load(imageRes)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            }).into(t)
}

fun <T : ImageView> setImage(c: View, imageRes: Int, progressBar: ProgressBar, t: T) {
    progressBar.visible()
    Glide.with(c)
            .load(imageRes)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            }).into(t)
}

fun <T : ImageView> setImage(c: View, url: String, progressBar: ProgressBar, t: T) {
    progressBar.visible()
    Glide.with(c)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            }).into(t)
}

fun <T : ImageView> setImageUrl(c: Context, imageRes: Int, progressBar: ProgressBar, errorResourceId: Int, t: T) {
    val options = RequestOptions()
            .error(errorResourceId)

    progressBar.visible()
    Glide.with(c)
            .load(imageRes)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: Context, imageRes: Int, progressBar: ProgressBar, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .error(errorDrawable)
    progressBar.visible()
    Glide.with(c)
            .load(imageRes)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}


fun <T : ImageView> setImageUrl(c: Context, url: String, t: T) {
    Glide.with(c)
            .load(url)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: Context, url: String, placeholderResourceId: Int, t: T) {
    val options = RequestOptions()
            .error(placeholderResourceId)
    Glide.with(c)
            .load(url)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: Context, url: String, placeHolderResourceId: Int, errorResourceId: Int, t: T) {
    val options = RequestOptions()
            .placeholder(placeHolderResourceId)
            .error(errorResourceId)

    Glide.with(c)
            .load(url)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: Context, url: String, placeHolderDrawable: Int, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .placeholder(placeHolderDrawable)
            .error(errorDrawable)
    Glide.with(c)
            .load(url)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: Context, url: String, progressBar: ProgressBar, t: T) {
    progressBar.visible()
    Glide.with(c)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .into(t)
}

fun <T : ImageView> setImageUrl(c: Context, url: String, progressBar: ProgressBar, errorResourceId: Int, t: T) {
    val options = RequestOptions()
            .error(errorResourceId)

    progressBar.visible()
    Glide.with(c)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: Context, url: String, progressBar: ProgressBar, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .error(errorDrawable)
    progressBar.visible()
    Glide.with(c)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}
fun <T : ImageView> setImageUrl(c: View, imageRes: Int, progressBar: ProgressBar, errorResourceId: Int, t: T) {
    val options = RequestOptions()
            .error(errorResourceId)

    progressBar.visible()
    Glide.with(c)
            .load(imageRes)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: View, imageRes: Int, progressBar: ProgressBar, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .error(errorDrawable)
    progressBar.visible()
    Glide.with(c)
            .load(imageRes)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImage(c: View, url: String, progressBar: ProgressBar, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .error(errorDrawable)
    progressBar.visible()
    Glide.with(c)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: View, url: String, t: T) {
    Glide.with(c)
            .load(url)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: View, url: String, placeholderResourceId: Int, t: T) {
    val options = RequestOptions()
            .error(placeholderResourceId)
    Glide.with(c)
            .load(url)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: View, url: String, placeHolderResourceId: Int, errorResourceId: Int, t: T) {
    val options = RequestOptions()
            .placeholder(placeHolderResourceId)
            .error(errorResourceId)

    Glide.with(c)
            .load(url)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: View, url: String, placeHolderDrawable: Int, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .placeholder(placeHolderDrawable)
            .error(errorDrawable)
    Glide.with(c)
            .load(url)
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: View, url: String, progressBar: ProgressBar, t: T) {
    progressBar.visible()
    Glide.with(c)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .into(t)
}

fun <T : ImageView> setImageUrl(c: View, url: String, progressBar: ProgressBar, errorResourceId: Int, t: T) {
    val options = RequestOptions()
            .error(errorResourceId)

    progressBar.visible()
    Glide.with(c)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}

fun <T : ImageView> setImageUrl(c: View, url: String, progressBar: ProgressBar, errorDrawable: Drawable, t: T) {
    val options = RequestOptions()
            .error(errorDrawable)
    progressBar.visible()
    Glide.with(c)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    progressBar.gone()
                    return false
                }
            })
            .apply(options)
            .into(t)
}