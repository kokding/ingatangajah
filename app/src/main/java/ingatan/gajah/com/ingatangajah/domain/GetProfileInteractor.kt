package ingatan.gajah.com.ingatangajah.domain

import ingatan.gajah.com.ingatangajah.repository.ApiRepository
import ingatan.gajah.com.ingatangajah.repository.response.ProfileResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class GetProfileInteractor(){
    private val apiManager by lazy {
        ApiRepository.createApiService()
    }

    fun getProfile(token: String): Observable<Response<ProfileResponse>> {

        return apiManager
                .getProfile(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}