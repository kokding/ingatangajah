package ingatan.gajah.com.ingatangajah.view.leaderboard

import ingatan.gajah.com.ingatangajah.repository.response.Data
import ingatan.gajah.com.ingatangajah.repository.response.PersonalLeaderboard
import ingatan.gajah.com.ingatangajah.view.adapter.model.Leaderboard

interface LeaderboardView{
    fun error(message: String?)
    fun hideLoading()
    fun showData(leaderboards: List<Leaderboard>?)
    fun showLoading()
    fun showDataPersonal(personalLeaderboard: PersonalLeaderboard?, userCredential: Data?)

}