package ingatan.gajah.com.ingatangajah.view.leaderboard

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.GetLeaderboardInteractor
import ingatan.gajah.com.ingatangajah.domain.GetPersonalLeaderboardInteractor
import ingatan.gajah.com.ingatangajah.repository.response.LeaderboardResponse
import ingatan.gajah.com.ingatangajah.repository.response.PersonalLeaderboardRsponse
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import io.reactivex.observers.DisposableObserver
import org.jetbrains.anko.AnkoLogger
import retrofit2.Response

class LeaderboardPresenter(val view: LeaderboardView, val disposableManager: DisposableManager, val context: Context?) : AnkoLogger {

    private val getLeaderboardInteractor: GetLeaderboardInteractor = GetLeaderboardInteractor()
    private val getPersonalLeaderboardInteractor: GetPersonalLeaderboardInteractor = GetPersonalLeaderboardInteractor()
    val preferenceManager: PreferenceManager = PreferenceManager(context!!)

    fun getLeaderboard(gameId: Int?) {
        view.showLoading()
        val dispose = getLeaderboardInteractor.getLeaderboard(gameId, "Bearer " + preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<LeaderboardResponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<LeaderboardResponse>) {
                view.hideLoading()
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.showData(t.body()?.leaderboards)
                    }
                }
            }

            override fun onError(e: Throwable) {
                view.hideLoading()
                view.error(e.message)
            }
        })
        disposableManager.add(dispose)
    }

    fun getPersonalLeaderboard(gameId: Int?) {
//        view.showLoading()
        val dispose = getPersonalLeaderboardInteractor.getLeaderboard(gameId, "Bearer " + preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<PersonalLeaderboardRsponse>>() {
            override fun onComplete() {
            }

            override fun onNext(t: Response<PersonalLeaderboardRsponse>) {
//                view.hideLoading()
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.showDataPersonal(t.body()?.personalLeaderboard, preferenceManager.getUserCredential())
                    }
                }
            }

            override fun onError(e: Throwable) {
//                view.hideLoading()
                view.error(e.message)
            }
        })
        disposableManager.add(dispose)
    }

}