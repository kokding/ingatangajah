package ingatan.gajah.com.ingatangajah.view.game.creategame

import ingatan.gajah.com.ingatangajah.repository.model.History
import ingatan.gajah.com.ingatangajah.repository.response.*
import retrofit2.Response

interface CreateGameView {
    fun error(message: String?)
    fun goToGame(question: GameFaceResponse?, randomQuestion: GameFaceResponse?, memorize: String, recall: String)
    fun showDataPersonal(personalLeaderboard: PersonalLeaderboard?, userCredential: Data?)
    fun hideLoadingHistory()
    fun showLoadingHistory()
    fun showHistoryList(histories: List<History>?, meta: Meta?)
    fun goToGameNumber(t: Response<GameNumberResponse>, body: GameNumberResponse?, memorize: String, recall: String)
    fun goToGameImage(question: GameImageResponse?, randomQuestion: GameImageResponse, memorize: String, recall: String)
    fun goToGameWord(body: GameWordResponse, wordRandom: GameWordResponse, memorize: String, recall: String)
}