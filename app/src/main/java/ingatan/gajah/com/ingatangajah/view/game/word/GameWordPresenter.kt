package ingatan.gajah.com.ingatangajah.view.game.word

import android.content.Context
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.domain.PostWordAnswerInteractor
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.request.GameImageAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.request.GameWordAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameImageResultResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResultResponse
import ingatan.gajah.com.ingatangajah.repository.sharedpreference.PreferenceManager
import ingatan.gajah.com.ingatangajah.view.game.order.GameOrderView
import io.reactivex.observers.DisposableObserver
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import retrofit2.Response

class GameWordPresenter(val view: GameWordView, val disposable: DisposableManager, context: Context) : AnkoLogger {
    val preferenceManager: PreferenceManager = PreferenceManager(context)
    val postGameWordAnswerInteractor: PostWordAnswerInteractor = PostWordAnswerInteractor()

    fun postAnswer(answer: GameWordAnswerRequest) {
        val dispose = postGameWordAnswerInteractor.postWordAnswer(AppConstant.GAME_WORDS,
                answer, preferenceManager.getAnonymousToken()).subscribeWith(object : DisposableObserver<Response<GameWordResultResponse>>(){
            override fun onComplete() {
            }

            override fun onNext(t: Response<GameWordResultResponse>) {
                info("Result"+t.body().toString())
                when (t.code()) {
                    400, 401, 404, 204 -> view.error(t.message())
                    else -> {
                        view.goToResult(t.body())
                    }
                }

            }

            override fun onError(e: Throwable) {
            }
        })
        disposable.add(dispose)
    }
}