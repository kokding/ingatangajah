package ingatan.gajah.com.ingatangajah.repository.response

import ingatan.gajah.com.ingatangajah.repository.model.Schedule
import com.google.gson.annotations.SerializedName

data class ScheduleResponse(
        @SerializedName("events") var schedules: List<Schedule>
)