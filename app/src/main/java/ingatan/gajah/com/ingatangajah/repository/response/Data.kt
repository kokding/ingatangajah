package ingatan.gajah.com.ingatangajah.repository.response

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
        
        @SerializedName("id")
        val id: String?,
        
        @SerializedName("email")
        val email: String?,
        
        @SerializedName("is_verified")
        val isVerified: Int?,
        
        @SerializedName("is_suspend")
        val isSuspend: Int?,
        
        @SerializedName("created_at")
        val createdAt: String?,
        
        @SerializedName("profile")
        var profile: Profile?) : Parcelable