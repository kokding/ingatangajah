package ingatan.gajah.com.ingatangajah.repository.request

data class LoginRequest(
        val email: String,
        val password: String
)
