package ingatan.gajah.com.ingatangajah.view.game.order

import ingatan.gajah.com.ingatangajah.repository.response.GameImageResultResponse

interface GameOrderView{
    fun goToResult(body: GameImageResultResponse?)
    fun error(message: String?)
}