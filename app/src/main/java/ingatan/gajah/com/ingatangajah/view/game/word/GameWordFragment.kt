package ingatan.gajah.com.ingatangajah.view.game.word


import android.support.v4.view.ViewPager
import ingatan.gajah.com.ingatangajah.R
import ingatan.gajah.com.ingatangajah.base.BaseFragment
import ingatan.gajah.com.ingatangajah.base.DisposableManager
import ingatan.gajah.com.ingatangajah.enum.AppConstant
import ingatan.gajah.com.ingatangajah.repository.model.Word
import ingatan.gajah.com.ingatangajah.repository.request.GameWordAnswerRequest
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResponse
import ingatan.gajah.com.ingatangajah.repository.response.GameWordResultResponse
import ingatan.gajah.com.ingatangajah.utils.MessageFactory
import ingatan.gajah.com.ingatangajah.view.adapter.GameWordAdapter
import ingatan.gajah.com.ingatangajah.view.adapter.model.GameOrder
import ingatan.gajah.com.ingatangajah.view.adapter.viewpager.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_game_word.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.info
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

class GameWordFragment : BaseFragment(), GameWordView, GameWordActivity.GameEndListener, GameWordAdapter.GameWordViewHolder.OnWordChangedListener,AnkoLogger {

    var gameOrder: MutableList<GameOrder> = mutableListOf()
    var answers: MutableList<Word> = mutableListOf()
    private val disposableManager = DisposableManager()
    private lateinit var presenter: GameWordPresenter
    lateinit var data: GameWordResponse
    private var state: Int = AppConstant.STATE_MEMORIZE
    private lateinit var vpAdapter: ViewPagerAdapter

    override fun getLayoutResource(): Int = R.layout.fragment_game_word

    companion object {

        fun newInstance(category: GameWordResponse, state: Int) = GameWordFragment().apply {
            arguments = bundleOf(
                    AppConstant.KEY_GAME_WORDS to category,
                    AppConstant.KEY_GAME_STATE to state)
        }
    }

    override fun initLib() {
        presenter = GameWordPresenter(this, disposableManager, context!!)
        GameWordActivity.setGameListener(this)
        GameWordAdapter.GameWordViewHolder.setWordChangedListner(this)
    }

    override fun initIntent() {
        data = arguments?.getParcelable(AppConstant.KEY_GAME_WORDS)!!
        state = arguments?.getInt(AppConstant.KEY_GAME_STATE, AppConstant.STATE_MEMORIZE)!!
        info{"word fragment state $state"}
    }

    override fun initUI() {
        initData()
        vpAdapter = ViewPagerAdapter(childFragmentManager, data,gameOrder,state, data.gameSettings.columns)
        vpWord.adapter = vpAdapter
        tvCounter.text = "1/${data.gameSettings.columns}"
        vpWord.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{

            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                tvCounter.text = "${position+1}/${data.gameSettings.columns}"
            }
        })
    }

    private fun initData() {
        data.data.forEach {
            gameOrder.add(GameOrder(
                    id = it.id,
                    img = "",
                    question = it.name,
                    answer = ""))
        }
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

    override fun onGameEnd(userMemorizeTime: Long, userRecallTime: Long) {
        MessageFactory.showDoneAlert(context,
                getString(R.string.title_end_game),
                getString(R.string.message_end_game_to_result),
                getString(R.string.btn_end_game_to_result),
                object : MessageFactory.Companion.MyDialogActionListener {
                    override fun onPositiveAnswer() {
//                        toast(gameOrder.toString())
                        gameOrder.forEach {
                            answers.add(Word(it.id,
                                    if (it.answer.isNullOrBlank()) "-1"
                                    else it.answer ?: "-1"))
                        }
                        data.gameSettings.userMemorize = userMemorizeTime.toInt()
                        data.gameSettings.userRecall = userRecallTime.toInt()
                        val answer = GameWordAnswerRequest(data.gameSettings, data.data, answers)
                        info("MANTAP " + answer.toString())
                        presenter.postAnswer(answer)
                    }

                    override fun onNegativeAnswer() {
                    }
                })
    }

    override fun onVisibleMemorizeClicked(isHide: Boolean) {
    }

    override fun error(message: String?) {
        toast(message ?: "Terjadi Kesalahan, Coba Beberapa saat lagi")

    }

    override fun onPause() {
        super.onPause()
        disposableManager.dispose()
    }

    override fun goToResult(body: GameWordResultResponse?) {
        startActivity<GameWordResultActivity>(AppConstant.KEY_RESULT to body)
        activity?.finish()
    }

    override fun changeAnswer(position: Int, answer: String?) {
            gameOrder.get(position).answer = answer
    }
}
