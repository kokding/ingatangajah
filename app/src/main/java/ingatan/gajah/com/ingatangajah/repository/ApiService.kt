package ingatan.gajah.com.ingatangajah.repository

import ingatan.gajah.com.ingatangajah.repository.request.*
import ingatan.gajah.com.ingatangajah.repository.response.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("app")
    fun getAnonymousToken(@Field("client_secret") secret: String): Observable<retrofit2.Response<ResponseModel>>

    @POST("login")
    fun login(@Body loginRequest: LoginRequest,
              @Header("Authorization") loginToken: String): Observable<retrofit2.Response<LoginResponse>>

    @POST("register")
    fun register(@Body registerRequest: RegisterRequest,
                 @Header("Authorization") loginToken: String): Observable<retrofit2.Response<ResponseModel>>

    @GET("games/{game_id}/question")
    fun getGameFaceQuestion(@Path("game_id") gameId: Int,
                            @Header("Authorization") loginToken: String,
                            @Query("columns") colum: String,
                            @Query("rows") rows: String,
                            @Query("memorize_time") memorizeTime: String,
                            @Query("recall_time") recall: String): Observable<Response<GameFaceResponse>>

    @POST("games/{game_id}/random")
    fun getRandomFaceQuestion(@Path("game_id") gameId: Int,
                              @Header("Authorization") loginToken: String,
                              @Body question: RandomRequest): Observable<Response<GameFaceResponse>>

    @POST("games/{game_id}/answer")
    fun postFaceAnswer(@Path("game_id") gameId: Int,
                       @Header("Authorization") loginToken: String,
                       @Body answerRequest: GameFaceAnswerRequest): Observable<Response<GameFaceResultResponse>>

    @GET("leaderboards/{game_id}")
    fun getLeaderboard(
            @Header("Authorization") loginToken: String,
            @Path("game_id") gameId: Int?): Observable<Response<LeaderboardResponse>>

    @GET("profiles/leaderboards/{game_id}")
    fun getPersonalLeaderboard(
            @Header("Authorization") loginToken: String,
            @Path("game_id") gameId: Int?): Observable<Response<PersonalLeaderboardRsponse>>

    @GET("games/{game_id}/history")
    fun getGameHistory(
            @Header("Authorization") loginToken: String,
            @Path("game_id") gameId: Int?,
            @Query("limit") limit: Int?): Observable<Response<HistoryResponse>>

    @Multipart
    @POST("profiles/avatars")
    fun postAvatar(
            @Header("Authorization") loginToken: String,
            @Part avatar: MultipartBody.Part): Observable<Response<PostAvatarResponse>>

    @GET("profiles")
    fun getProfile(
            @Header("Authorization") loginToken: String): Observable<Response<ProfileResponse>>

    @GET("games/{game_id}/question")
    fun getGameNumberQuestion(@Path("game_id") gameId: Int,
                            @Header("Authorization") loginToken: String,
                            @Query("columns") colum: String,
                            @Query("rows") rows: String,
                            @Query("memorize_time") memorizeTime: String,
                            @Query("recall_time") recall: String): Observable<Response<GameNumberResponse>>

    @POST("games/{game_id}/answer")
    fun postGameNumberAnswer(@Path("game_id") gameId: Int,
                       @Header("Authorization") loginToken: String,
                       @Body answerRequest: GameNumberAnswerRequest): Observable<Response<GameNumberResultResponse>>

    @GET("games/{game_id}/question")
    fun getGameImageQuestion(@Path("game_id") gameId: Int,
                            @Header("Authorization") loginToken: String,
                            @Query("columns") colum: String,
                            @Query("rows") rows: String,
                            @Query("memorize_time") memorizeTime: String,
                            @Query("recall_time") recall: String): Observable<Response<GameImageResponse>>

    @POST("games/{game_id}/random")
    fun getGameImageRandomQuestion(@Path("game_id") gameId: Int,
                              @Header("Authorization") loginToken: String,
                              @Body question: ImageRandomRequest): Observable<Response<GameImageResponse>>

    @POST("games/{game_id}/answer")
    fun postGameImageAnswer(@Path("game_id") gameId: Int,
                       @Header("Authorization") loginToken: String,
                       @Body answerRequest: GameImageAnswerRequest): Observable<Response<GameImageResultResponse>>

    @GET("games/{game_id}/question")
    fun getGameWordQuestion(@Path("game_id") gameId: Int,
                             @Header("Authorization") loginToken: String,
                             @Query("columns") colum: String,
                             @Query("rows") rows: String,
                             @Query("memorize_time") memorizeTime: String,
                             @Query("recall_time") recall: String): Observable<Response<GameWordResponse>>

    @POST("games/{game_id}/random")
    fun getGameWordRandomQuestion(@Path("game_id") gameId: Int,
                                   @Header("Authorization") loginToken: String,
                                   @Body question: WordRandomRequest): Observable<Response<GameWordResponse>>

    @POST("games/{game_id}/answer")
    fun postGameWordAnswer(@Path("game_id") gameId: Int,
                            @Header("Authorization") loginToken: String,
                            @Body answerRequest: GameWordAnswerRequest): Observable<Response<GameWordResultResponse>>
}